function Helpers() {
};

Helpers.getRandomInt = function (from, length) {
    // Math.random() between 0 (inclusive) and 1 (exclusive)
    return Math.floor(Math.random() * length) + from;
}

Helpers.createPrivateKey = function () {
    return Math.random().toString(36).substr(2, 20);
}

// shim for IE8
if (!Date.now) {
    Date.now = function () {
        return new Date().getTime();
    }
}
Helpers.getCurrentTimestampInSeconds = function () {
    return Math.floor(Date.now() / 1000);
}

Helpers.getRoute = function (route, lang) {
    return ROUTES[route].langs[lang];
}

Helpers.getDomainLang = function () {
    var lang = window.location.host.substr(0, window.location.host.indexOf('.'));
    if (lang in TRANSLATION_LANGUAGES) {
        return lang;
    } else {
        // if it's unknown by default page displayed in english
        return 'en';
    }
}

Helpers.getMainDomain = function (lang, preserveLang) {
    var host = window.location.host + "/";
    if (host[2] === '.' && preserveLang !== true) {
        // so it's already language subdomain
        host = host.slice(3);
    }
    if (host.indexOf('localhost') === 0) {
        // it's on localhost so add the folder hardcoded
        if (host.indexOf('dist') > 0) {
            host += HOME + "/dist/";
        } else {
            host += HOME + "/app/";
        }
    }
    if (lang) {
        host = lang + "." + host;
    }
    return 'http://' + host;
}
Helpers.getMainDomainWithCurrentLang = function () {
    //don't remove the current lang subdomain
    return Helpers.getMainDomain(null, true);
}

Helpers.redirectToMainDomain = function (newRoute) {
    window.location.href = Helpers.getMainDomain() + newRoute;
}

Helpers.redirectToSubdomain = function (newRoute, lang) {
    window.location.href = Helpers.getMainDomain(lang) + newRoute;
}

Helpers.shouldShowPersonalMessages = function (routeKey) {
    if (routeKey === undefined) {
        return false;
    }
    var index = SHOULD_OMIT_NO_LOGIN_MESSAGE.indexOf(routeKey);
    return index < 0;
}


Helpers.toIds = function (arrayWithIds) {
    var result = [];
    for (var key in arrayWithIds) {
        result.push(arrayWithIds[key].id);
    }
    return result;
}

String.prototype.hashCode = function () {
    var hash = 0, i, chr, len;
    if (this.length === 0) return hash;
    for (i = 0, len = this.length; i < len; i++) {
        chr = this.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
};

$(function () {
    // jquery hack to avoid back on backspace when test finished
    $(document).on("keydown", function (e) {
        if (e.which === 8 && !$(e.target).is("input, textarea")) {
            e.preventDefault();
        }
    });
});