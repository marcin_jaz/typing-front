var DOUBLE_KEYSTROKES = 'ąćęłńóśźżíáéúüûîâüÿœëçï:"!?()ё';

var L1 = "l1";
var L2 = "l2";
var L3 = "l3";
var L4 = "l4";
var L5 = "l5";
var R1 = "r1";
var R2 = "r2";
var R3 = "r3";
var R4 = "r4";
var R5 = "r5";

// REMEMBER! put always modifier (R1 or L1 as the second - on base of it in
// directive the shift is chosen)
var KEYBOARD = new Array();
KEYBOARD[' '] = [L1, R1];

// all polish
KEYBOARD['a'] = [L5];
KEYBOARD['d'] = [L3];
KEYBOARD['r'] = [L2];
KEYBOARD['w'] = [L4];

KEYBOARD['j'] = [R2];
KEYBOARD['k'] = [R3];
KEYBOARD['o'] = [R4];

KEYBOARD['i'] = [R3];
KEYBOARD['s'] = [L4];

KEYBOARD['e'] = [L3];
KEYBOARD['u'] = [R2];

KEYBOARD['t'] = [L2];
KEYBOARD['y'] = [R2];

KEYBOARD['p'] = [R5];
KEYBOARD['z'] = [L5];

KEYBOARD['f'] = [L2];
KEYBOARD['n'] = [R2];

KEYBOARD['b'] = [L2];
KEYBOARD['l'] = [R4];

KEYBOARD['c'] = [L3];
KEYBOARD['m'] = [R2];

KEYBOARD['g'] = [L2];
KEYBOARD['h'] = [R2];

KEYBOARD['q'] = [L5];
KEYBOARD['x'] = [L4];
KEYBOARD['v'] = [L2];

KEYBOARD['ą'] = [L5, R1];
KEYBOARD['ę'] = [L3, R1];
KEYBOARD['ł'] = [R4, R1];
KEYBOARD['ó'] = [R4, R1];

KEYBOARD['ć'] = [L3, R1];
KEYBOARD['ń'] = [R2, R1];
KEYBOARD['ż'] = [L5, R1];
KEYBOARD['ź'] = [L4, R1];
KEYBOARD['ś'] = [L4, R1];

KEYBOARD[','] = [R3];
KEYBOARD['.'] = [R4];
KEYBOARD['?'] = [L5, R5];
KEYBOARD['!'] = [L5, R5];
KEYBOARD['-'] = [R4];
KEYBOARD['('] = [L5, R3];
KEYBOARD[')'] = [L5, R4];
KEYBOARD[';'] = [R5];
KEYBOARD[':'] = [L5, R5];
KEYBOARD['"'] = [L5, R5];

// GERMAN special
KEYBOARD['y|de'] = [L5];
KEYBOARD['z|de'] = [R2];

KEYBOARD['ü|de'] = [R5];
KEYBOARD['ä|de'] = [R5];
KEYBOARD['ß|de'] = [R5];// za zerem
KEYBOARD['ö|de'] = [R5];

KEYBOARD['z|de'] = [R2];
KEYBOARD['y|de'] = [L2];
KEYBOARD['(|de'] = [L5, R2];
KEYBOARD[')|de'] = [L5, R3];

// SPANISH special "óíáé", "ñúü"
KEYBOARD['ó|es'] = [R5, R4];// R5 is ´ to push first
KEYBOARD['í|es'] = [R5, R3];
KEYBOARD['á|es'] = [R5, L5];
KEYBOARD['é|es'] = [R5, L3];

KEYBOARD['ñ|es'] = [R5];
KEYBOARD['ú|es'] = [R5, R2];
// one on the right of ñ with shift, then u, for uppercase all 3 together
KEYBOARD['ü|es'] = [R5, L5, R2];
KEYBOARD['¿|es'] = [L5, R5];
KEYBOARD['¡|es'] = [R5];
KEYBOARD['-|es'] = [R5];
KEYBOARD['(|es'] = [L5, R2];
KEYBOARD[')|es'] = [L5, R3];
KEYBOARD[':|es'] = [L5, R4];
KEYBOARD['"|es'] = [L4, R5];

// FRENCH special letters "àèêô", "ûîâü", "ùœëçï"
KEYBOARD['à|fr'] = [R4];
KEYBOARD['è|fr'] = [R2];
KEYBOARD['é|fr'] = [L4];
KEYBOARD['ê|fr'] = [R5];
KEYBOARD['ô|fr'] = [R5];

KEYBOARD['û|fr'] = [R5, R2];
KEYBOARD['î|fr'] = [R5, R3];
KEYBOARD['â|fr'] = [R5, L5];
KEYBOARD['ü|fr'] = [L1, R5, R2];
KEYBOARD['ÿ|fr'] = [L1, R5, R2];

KEYBOARD['ù|fr'] = [R5];
// Ctrl+⇧ Shift+& then o in quick succession o - didn't work here...
KEYBOARD['œ|fr'] = [L5, R5, L4, R4];
KEYBOARD['ë|fr'] = [L1, R5, L3];
KEYBOARD['ç|fr'] = [R3];
KEYBOARD['ï|fr'] = [L1, R5, R3];

KEYBOARD['z|fr'] = [L4];
KEYBOARD['w|fr'] = [L5];
KEYBOARD['m|fr'] = [R5];

KEYBOARD[',|fr'] = [R2];
KEYBOARD['.|fr'] = [L5, R3];
KEYBOARD['?|fr'] = [L5, R2];
KEYBOARD['!|fr'] = [R5];
KEYBOARD['-|fr'] = [L2];
KEYBOARD['(|fr'] = [L2];
KEYBOARD[')|fr'] = [R5];
KEYBOARD[';|fr'] = [R3];
KEYBOARD[':|fr'] = [R4];
KEYBOARD['"|fr'] = [L3];

// CROATIAN čć", "đšž
KEYBOARD['č|hr'] = [R5];
KEYBOARD['ć|hr'] = [R5];
KEYBOARD['đ|hr'] = [R5];
KEYBOARD['š|hr'] = [R5];
KEYBOARD['ž|hr'] = [R5];

KEYBOARD['z|hr'] = [R2];
KEYBOARD['y|hr'] = [L2];

KEYBOARD['?|hr'] = [L5, R5];
KEYBOARD['-|hr'] = [L5, R5];
KEYBOARD['(|hr'] = [L5, R2];
KEYBOARD[')|hr'] = [L5, R3];
KEYBOARD[';|hr'] = [L5, R3];
KEYBOARD[':|hr'] = [L5, R4];
KEYBOARD['"|hr'] = [L4, R5];

//ITALIAN specials
KEYBOARD['?|it'] = [L5, R5];
KEYBOARD['-|it'] = [L5, R5];
KEYBOARD['(|it'] = [L5, R2];
KEYBOARD[')|it'] = [L5, R3];
KEYBOARD[';|it'] = [L5, R3];
KEYBOARD[':|it'] = [L5, R4];
KEYBOARD['"|it'] = [L4, R5];

//Russian
KEYBOARD['о'] = [R2];
KEYBOARD['н'] = [R2];
KEYBOARD['т'] = [R2];
KEYBOARD['е'] = [L2];
KEYBOARD['с'] = [L3];
KEYBOARD['а'] = [L2];
KEYBOARD['и'] = [L2];
KEYBOARD['р'] = [R2];
KEYBOARD['в'] = [L3];
KEYBOARD['ь'] = [R2];
KEYBOARD['л'] = [R3];
KEYBOARD['к'] = [L2];
KEYBOARD['п'] = [L2];
KEYBOARD['д'] = [R4];
KEYBOARD['й'] = [L5];
KEYBOARD['ы'] = [L4];
KEYBOARD['я'] = [L5];
KEYBOARD['у'] = [L3];
KEYBOARD['м'] = [L2];
KEYBOARD['з'] = [R5];
KEYBOARD['б'] = [R3];
KEYBOARD['ч'] = [L4];
KEYBOARD['г'] = [R2];
KEYBOARD['ж'] = [R5];
KEYBOARD['ш'] = [R3];
KEYBOARD['х'] = [R5];
KEYBOARD['ц'] = [L4];
KEYBOARD['щ'] = [R4];
KEYBOARD['ё'] = [R1, L2];
KEYBOARD['ю'] = [R4];
KEYBOARD['ф'] = [L5];
KEYBOARD['э'] = [R4];
KEYBOARD['ъ'] = [R5];
//Romanian ăâîșț
