angular.module('myApp.filters', []).filter('trusted', ['$sce', function ($sce) {
    return function (url) {
        return $sce.trustAsResourceUrl(url);
    };
}]).filter('unsafe', ['$sce', function ($sce) {
    return $sce.trustAsHtml;
}]).filter('writeConfig', ['translateFilter', function (translateFilter) {
    return function (number) {
        if (number > 5000) {
            return translateFilter('NO_LIMIT');
        } else {
            return number;
        }
    };
}]).filter('learnConfigDisplay', ['translateFilter', function (translateFilter) {
    return function (config) {
        if (config !== undefined) {
            var time = config.learn_minutes + translateFilter('MINUTE_ONE_LETTER');
            var letters = (config.learn_config + 1) + '/' + config.letters_number;
            var focus = config.use_focus == 1 ? SIGN_TRUE : SIGN_FALSE;
            return time + ', ' + letters + ', ' + focus;
        }
        return config;
    };
}]).filter('writeConfigDisplay', ['translateFilter', 'writeConfigFilter', function (translateFilter, writeConfigFilter) {
    return function (config) {
        if (config !== undefined) {
            var time = config.write_minutes + translateFilter('MINUTE_ONE_LETTER');
            var letters = writeConfigFilter(config.write_config);
            var focus = config.use_focus == 1 ? SIGN_TRUE : SIGN_FALSE;
            return time + ', ' + letters + ', ' + focus;
        }
        return config;
    };
}]).filter('gameConfigDisplay', function () {
    return function (config) {
        if (config !== undefined) {
            var letters = (config.learn_config + 1) + '/' + config.letters_number;
            var focus = config.use_focus == 1 ? SIGN_TRUE : SIGN_FALSE;
            return letters + ', ' + focus;
        }
        return config;
    };
}).filter('sortMark', function () {
    return function (sorted) {
        if (sorted === 0) {
            return '';
        } else if (sorted === 2) {
            return ARROW_UP;
        } else if (sorted === 1) {
            return ARROW_DOWN;
        }
    };
}).filter('fromNow', ['$moment', function ($moment) {
    return function (time) {
        if (!time) {
            return '-';//undefined
        }
        return $moment(time).fromNow();
    };
}]).filter('durationInMinutes', ['$moment', function ($moment) {
    return function (time) {
        //TODO MJ customize this duration to display hours and minutes together - for stats
        if (!time) {
            return 0;
        }
        return $moment.duration(time, "minutes").humanize();
    };
}]).filter('groupCompetitionConfig', ['writeConfigFilter', 'translateFilter', function (writeConfigFilter, translateFilter) {
    return function (c) {
        var time = c.typing_minutes + translateFilter('MINUTE_ONE_LETTER');
        var words = writeConfigFilter(c.typing_words) + ' ' + translateFilter('WORDS');
        return time + ', ' + words;
    };
}]).filter('competitionJoinLink', ['userService', function (userService) {
    return function (c) {
        return userService.getRoute('competition') + '/' + c.id + "/" + c.private_key
    };
}]).filter('groupUsersCount', ['filterFilter', function (filterFilter) {
    return function (students, searchString) {
        if (students === undefined) {
            return '';
        }
        var originalLength = students.length;
        var filteredLength = filterFilter(students, searchString).length;
        if (originalLength !== filteredLength) {
            return '(' + filteredLength + '/' + originalLength + ')';
        }
        return '(' + originalLength + ')';
    };
}]).filter('openClosedCompetition', function () {
    return function (privateKey) {
        if (privateKey.length > 0) {
            return "close";
        }
        return "open";
    };
}).filter('langName', function () {
    return function (short) {
        return WRITING_LANGUAGES[short];
    };
}).filter('fromNow', ['$moment', function ($moment) {
    return function (date) {
        if (date === 0) {
            return '-';
        }
        return $moment(date).fromNow();
    };
}]).filter('toCalendar', ['$moment', function ($moment) {
    return function (date) {
        return $moment(date).calendar();
    };
}]).filter('competitionEndToCalendar', ['$moment', function ($moment) {
    return function (endTime) {
        return $moment(endTime).calendar();
    };
}]).filter('competitionEndFromNow', ['$moment', function ($moment) {
    return function (endTime) {
        return $moment(endTime).fromNow();
    };
}])
;


