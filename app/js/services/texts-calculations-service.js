'use strict';

function TextsCalculationsService() {

    this.retrieveLetterFromForLearn = function (lettersConfig, config) {
        var letterFrom = lettersConfig;
        // console.log("letterFrom=" + letterFrom);
        if (lettersConfig == LAST_LETTER_INDEX_ALWAYS_INCLUDED) {
            letterFrom = 0;
            // letterFrom = letterFrom - LAST_LETTER_INDEX_ALWAYS_INCLUDED + 1;
        } else if (config.lang === 'fr' && lettersConfig > 35) {
            // for french, for special letters it's adding too little
            letterFrom = lettersConfig - 3;
        } else if (config.lang === 'es' && lettersConfig == 32) {
            // for spanish, for last letter it's adding too little
            letterFrom = lettersConfig - 1;
        }
        return letterFrom;
    }

    this.createSpecialChars = function (specials) {
        var r = [];
        for (var i = 0; i < specials.length; i++) {
            r.push(new SpecialChar(specials[i]));
        }
        return r;
    }

    this.getFullCharsFromSpecials = function (specialChars) {
        var r = [];
        for (var k in specialChars) {
            r.push(specialChars[k].content);
        }
        return r;
    }

    var getCharsFromSpecials = function (specialChars) {
        var r = [];
        for (var k in specialChars) {
            r.push(specialChars[k].content.charAt(0));
        }
        return r;
    }


    var isFocusLetterAvailable = function (f) {
        return f !== undefined && f !== "" && f !== " ";
    }

    var isLetterFocusPossibleForLearn = function (letters, config) {
        //letters come with special chars included and special chars can't be set here on focus here!
        //only the letters that are already added can be in focus
        var indexOfFocusLetter = letters.indexOf(config.focus_letter);
        return indexOfFocusLetter >= 0 && indexOfFocusLetter <= config.learn_config;
    }

    this.shouldAddFocusForLearn = function (letters, config) {
        return this.shouldAddFocus(config) && isLetterFocusPossibleForLearn(letters, config);
    }

    this.shouldAddFocus = function (config) {
        return config.use_focus == 1 && isFocusLetterAvailable(config.focus_letter)
    }

    var shuffleArray = function (array) {
        for (var i = array.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
    }

    this.shuffleArrayAndCut = function (array, limit) {
        shuffleArray(array);
        var length = Math.min(array.length, limit);
        return array.slice(0, length);
    }

    this.chooseSpecialsToInsert = function (specials, toInsertCount, focusChar) {
        var result = [];

        //half of all is the last added special char - to practice
        var lastAddedToInsert = Math.ceil(toInsertCount / 2);
        for (var i = 0; i < lastAddedToInsert; i++) {
            result.push(specials[specials.length - 1]);
        }
        //console.log("LAST: " + lastAddedToInsert);

        var focusTimesToInsert = 0;
        var chars = getCharsFromSpecials(specials);
        if (focusChar in chars) {
            //means focus char is there and should be set, only 1 spot left for other
            focusTimesToInsert = toInsertCount - lastAddedToInsert - 1;
            if (specials.length > 4) {
                result.pop();//remove one
                lastAddedToInsert--;
                focusTimesToInsert--;
            }
        }
        for (var i = 0; i < focusTimesToInsert; i++) {
            result.push(specials[chars.indexOf(focusChar)]);
        }


        var othersToInsert = toInsertCount - lastAddedToInsert - focusTimesToInsert;
        if (specials.length > 4) {
            //insert one from before - otherwise it wouldn't be possible
            var randomIndex = Math.floor(Math.random() * (specials.length - 4));
            result.push(specials[randomIndex]);
            othersToInsert--;
        }

        var factor = specials.length > 1 ? 2 : 1;//fills with others if possible, otherwise includes the last
        var startOthersIndex = specials.length - factor;
        while (othersToInsert > 0) {
            //now one from the end - most recently added
            for (var i = startOthersIndex; i >= 0 && othersToInsert > 0; i--) {
                result.push(specials[i]);
                othersToInsert--;
            }
            startOthersIndex = specials.length - factor;//to start again from the last added
        }
        return result;
    }

    var createIndexesToInsert = function (toInsertCount, wordsLength) {
        var lastIndex = wordsLength - 2;//don't insert on the last to have possibility to close/uppercase
        var indexesToInsert = [0];//between second and first number below - gap between them to insert special char

        var allIndexes = [];
        for (var i = 2; i < lastIndex - 1; i++) {
            allIndexes.push(i);
        }
        for (var i = 0; i < toInsertCount - 2; i++) {
            var randomIndex = Math.floor(Math.random() * allIndexes.length);
            indexesToInsert.push(allIndexes.splice(randomIndex, 1)[0]);
        }
        //ascending order
        indexesToInsert.sort(function (a, b) {
            return a - b
        });
        indexesToInsert.push(lastIndex);//always include the last
        //console.log("INDEXES");
        //console.log(indexesToInsert);
        return indexesToInsert;
    }

    this.insertSpecialCharsBuildingString = function (words, specials, focusChar) {
        var toInsertCount = Math.ceil(words.length / 2.8);
        var specialsToInsert = this.chooseSpecialsToInsert(specials, toInsertCount, focusChar);
        shuffleArray(specialsToInsert);
        //console.log("after");
        //console.log(specialsToInsert);

        var indexesToInsert = createIndexesToInsert(toInsertCount, words.length);

        var nextSpecialIndex = indexesToInsert.shift();
        var closingOfPrevious = '';
        var text = '';
        for (var i = 0; i < words.length; i++) {
            text += words[i].v + closingOfPrevious;
            closingOfPrevious = '';
            if (i == nextSpecialIndex) {
                var special = specialsToInsert.shift();
                if (special.shouldNextBeUppercase()) {
                    words[i + 1].v = words[i + 1].v.charAt(0).toUpperCase() + words[i + 1].v.slice(1);
                }
                text += special.getTextToAppend();
                nextSpecialIndex = indexesToInsert.shift();
                closingOfPrevious = special.closeWith;
            } else {
                text += " ";
            }
        }
        //console.log(text);
        return text;
    }

}

angular.module('myApp.services').service('textsCalculationsService', TextsCalculationsService);