'use strict';

function AuthInterceptor($q, $location, errorService, $injector, localStorageService) {
    return {
        'request': function (config) {
            var token = localStorageService.getToken();
            if (token) {
                config.headers = config.headers || {};
                if (localStorageService) {
                    config.headers.XAuthorization = token + '-' + Helpers.getCurrentTimestampInSeconds() + '-' + localStorageService.getUser().id;
                }
            }
            return config;
        },
        'response': function (response) {
            return response || $q.when(response);
        },
        'responseError': function (rejection) {
            if (rejection.data) {
                var userService = $injector.get("userService");
                if (rejection.status === 401) {
                    userService.logout(function () {
                        //Helpers.redirectToMainDomain('index');
                        errorService.setError(rejection.data);
                        var $route = $injector.get("$route");
                        $route.reload();
                    });
                } else if (rejection.data.href) {
                    var href = userService.getRoute(rejection.data.href);
                    $location.path(href);
                    //errorService.setErrorWithRedirection(rejection.data);
                    errorService.setError(rejection.data);
                } else {
                    errorService.setError(rejection.data);
                }
            }
            return $q.reject(rejection);
        }
    };
}
angular.module('myApp.services').factory('authInterceptor', ['$q', '$location', 'errorService', '$injector', 'localStorageService', AuthInterceptor]);