'use strict';

function TypingConfigService($http, textsService) {

    var config = null;
    var hasChanged = false;

    var choosenLang = '';// for change in modal

    this.setNewLang = function (lang) {
        choosenLang = lang;
    }

    this.clean = function () {
        config = null;
    }

    var updateHasChanged =
        function (c) {
            hasChanged =
                c.write_config != config.write_config || c.learn_config != config.learn_config
                || c.learn_minutes != config.learn_minutes || c.write_minutes != config.write_minutes
                || c.use_focus != config.use_focus;
        }

    this.configHasChanged = function () {
        return hasChanged;
    }

    this.markConfigAsChanged = function () {
        hasChanged = true;
    }

    this.setConfig = function (c) {
        updateHasChanged(c);
        config = c;
    }

    this.getConfig = function () {
        return config;
    }

    var loadConfigForLang = function (user, lang, callback) {
        $http.get(REST + 'user/getConfig/' + user.id, {
            params: {
                lang: lang
            }
        }).success(function (c) {
            if (c.lang === "") {// in case it's default without language
                c.lang = user.site_lang;
            }
            config = c;
            choosenLang = c.lang;
            textsService.loadLanguageDataFile(c, callback);
        });
    }

    this.loadConfig = function (user, callback) {
        if (config == null) {// on login, logout, refresh
            // initialize(user.id, callback);
            loadConfigForLang(user, "", callback)
        } else if (choosenLang != config.lang) {// on change in modal
            loadConfigForLang(user, choosenLang, callback);
        } else { // when nothing changed use already loaded one
            callback(config);
        }
    }

    this.saveConfig = function () {
        if (config.user_id > 0) {
            $http.post(REST + 'user/saveConfig/', config).success(function () {
                hasChanged = false;
            });
        }
    }

    this.changeDefaultLanguage = function (lang) {
        if (config.user_id > 0) {
            $http.get(REST + 'user/markLangConfigAsLatest/' + config.user_id, {
                params: {
                    lang: lang
                }
            });
        }
    }

    this.makeLearnHarder = function () {
        if (config.learn_config < textsService.getLetters().length - 1) {
            config.learn_config++;
            this.markConfigAsChanged();
        }
        return config.learn_config;
    }

    this.setFocusLetter = function (letter) {
        config.focusLetter = letter;
    }

    this.makeLearnEasier = function () {
        if (config.learn_config > 5) {
            config.learn_config--;
            this.markConfigAsChanged();
        }
        return config.learn_config;
    }
}

angular.module('myApp.services').service('typingConfigService', ['$http', 'textsService', TypingConfigService]);