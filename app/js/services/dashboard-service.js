'use strict';

function DashboardService($translate, $filter) {

    this.buildChart = function (approaches) {
        var allConverted = [];
        var realThis = this;
        $translate(['TIME', 'KEYSTROKES', 'RESULT', 'LANGUAGE']).then(function (translations) {
            for (var i = approaches.length - 1; i >= 0; i--) {
                var converted = realThis._buildChartForApproach(approaches[i], translations);
                allConverted.push(converted);
            }
        });
        return allConverted;
    }

    this._buildChartForApproach =
        function (a, translations) {
            return {
                "c": [{
                    "v": $filter('fromNow')(a.date)
                }, {
                    "v": a.wpm
                }, {
                    "v": a.accuracy,
                    "f": a.accuracy + "%"
                }, {
                    "v": a.result
                }, {
                    "v": "<div class='apr'><div>" + translations.LANGUAGE + ": <img class='flag' src='./img/16/"
                    + a.lang + ".png'/> <b>" + $filter('langName')(a.lang) + "</b></div><div>"
                    + translations.TIME + ": <b>" + $filter('durationInMinutes')(a.typing_minutes) +
                    " </b></div><div>" + translations.KEYSTROKES + ": <b>" + a.keystrokes
                    + "</b></div><div>" + translations.RESULT + ": <b>" + a.result + "</b></div></div>"
                }]
            }
        }

}


angular.module('myApp.services').service('dashboardService', ['$translate', '$filter', DashboardService]);