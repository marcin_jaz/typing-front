angular.module('myApp.services').factory("stacktraceService", function () {
    // "printStackTrace" is a global object.
    return ({
        print: printStackTrace
    });
});

angular.module('myApp.services').provider("$exceptionHandler", {
    $get: ['errorLogService', function (errorLogService) {
        return (errorLogService);
    }]
});

var SENT_ERRORS = new Array();
function ErrorLogService($log, $window, $injector, stacktraceService, localStorageService) {

    // Log the given error to the remote server.
    function doLogError(exception, message, cause) {
        var userService = $injector.get('userService');
        var user = "";
        if (userService !== null) {
            user = JSON.stringify(userService.getCurrentUser());
        }

        var stacktrace = stacktraceService.print({
            e: exception
        });

        var messageHash = message.hashCode();
        if (SENT_ERRORS.indexOf(messageHash) < 0) {
            $.ajax({
                type: "POST",
                url: REST + 'logger/save/',
                contentType: "application/json",
                data: angular.toJson({
                    url: $window.location.href,
                    browser: $window.navigator.userAgent,
                    message: message,
                    stacktrace: stacktrace,
                    cause: (cause || ""),
                    user: user
                })
            });
            SENT_ERRORS.push(messageHash);
        }
    }

    function log(exception, cause) {

        // Pass off the error to the default error handler
        // on the AngualrJS logger. This will output the
        // error to the console (and let the application
        // keep running normally for the user).
        $log.error.apply($log, arguments);

        // Now, we need to try and log the error the server.
        // --
        // NOTE: In production, I have some debouncing
        // logic here to prevent the same client from
        // logging the same error over and over again! All
        // that would do is add noise to the log.
        try {

            var message = exception.toString();
            // Log the JavaScript error to the server
            // check if the message contains something, don't send tpload, until page reloaded don't send the same error more than once
            if (message !== undefined && message.trim().length > 0 && message.indexOf("$compile:tpload") < 0
                && message.indexOf("permission") < 0) {

                var $http = $injector.get('$http');
                $http.get(REST + 'version/get').success(function (currentVersion) {
                    if (localStorageService.get('version') == currentVersion) {
                        //log error only if js version is the same as on the server, otherwise hard refresh
                        doLogError(exception, message, cause);
                    } else {
                        localStorageService.put('version', currentVersion);
                        window.location.reload(true);
                    }
                });
            }

        } catch (loggingError) {

            // For Developers - log the log-failure.
            $log.warn("Error logging failed");
            $log.log(loggingError);

        }

    }

    // Return the logging function.
    return (log);
}

angular.module('myApp.services')
    .factory("errorLogService", ['$log', '$window', '$injector', 'stacktraceService', 'localStorageService', ErrorLogService]);