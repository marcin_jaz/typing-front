'use strict';

function Loader($timeout) {

    var showSpinner = false;
    var requested = 0;
    var canBeShownMilis = 200;
    var clock = null;

    var observerCallback;

    this.registerObserverCallback = function (callback) {
        observerCallback = callback;
    };

    this.start = function () {
        requested++;
        if (clock == null) {
            clock = $timeout(function () {
                window.clearInterval(clock);
                showSpinner = true;
                observerCallback(true);
            }, canBeShownMilis);
        }
    }

    this.noLoaderRequest = function () {
        requested++;
    }

    this.complete = function () {
        requested--;
        if (requested <= 0) {
            requested = 0;
            if (clock) {
                $timeout.cancel(clock);
                clock = null;
            }
            if (showSpinner === true) {
                showSpinner = false;
                observerCallback(false);
            }
        }
    }

    this.reset = function () {
        requested = 0;
    }

}

angular.module('myApp.services').service('loader', ['$timeout', Loader]);
