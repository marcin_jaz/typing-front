'use strict';

function LoaderInterceptor($q, loader) {
    return {
        request: function (config) {
            // if (config.method === "POST") {
            // loader.noLoaderRequest();
            // } else {
            loader.start();
            // }
            return config;
        },
        response: function (response) {
            loader.complete();
            return response || $q.when(response);
        },
        responseError: function (rejection) {
            loader.complete();
            return $q.reject(rejection);
        }
    };
}

angular.module('myApp.services').factory('loaderInterceptor', ['$q', 'loader', LoaderInterceptor]);