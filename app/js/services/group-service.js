'use strict';

function GroupService($http, localStorageService, competitionService, $filter) {

    this.getPositionsOfCompetitionsInGroup = function (groupId, callback) {
        competitionService.getPositionsOfCompetitionsInGroup(groupId, callback);
    }

    this.setFirstToDisplay = function (groupId) {
        localStorageService.put('visited_group', groupId);
    }

    this.getFirstToDisplay = function () {
        return localStorageService.getInt('visited_group');
    }

    this.removeFirstToDisplay = function () {
        return localStorageService.remove('visited_group');
    }

    this.setLastTab = function (tab) {
        localStorageService.put('group_tab', tab);
    }
    this.getLastTab = function () {
        return localStorageService.get('group_tab', 'overall');

    }

    this.getGroupById = function (groupId, callback) {
        $http.get(REST + 'group/getById/' + groupId).success(function (group) {
            callback(group);
        });
    }

    this.getStudentsWithStats = function (group, type, callback) {
        $http.post(REST + 'group/getStudentsWithStats?type=' + type, group).success(function (students) {
            callback(students);
        });
    };

    this.createGroup = function (group, callback) {
        group.privateKey = Helpers.createPrivateKey();
        $http.post(REST + 'group/save', group).success(function (id) {
            callback(id);
        });
    }

    this.updateGroup = function (group, callback) {
        $http.post(REST + 'group/update', group).success(function () {
            callback();
        });
    }

    this.changeName = function (group) {
        $http.post(REST + 'group/changeName', group);
    };

    this.addUserToGroup = function (groupId, userId, hash, callback, onFail) {
        $http.post(REST + 'group/addUser', {userId: userId, groupId: groupId, hash: hash}).success(function (added) {
            callback(added);
        }).error(function () {
            onFail();
        });
    }

    this.removeUserFromGroup = function (groupId, userId, creatorId, callback) {
        $http.post(REST + 'group/removeUser', {
            userId: userId,
            groupId: groupId,
            creatorId: creatorId
        }).success(function () {
            callback();
        });
    }

    this.getGroupsForUser = function (userId, callback) {
        $http.get(REST + 'group/getGroupsForUser/' + userId).success(function (assignment) {
            callback(assignment);
        });
    }

    this.getLastGroupIdForUser = function (userId, callback) {
        $http.get(REST + 'group/getLastGroupIdForUser/' + userId).success(function (groupId) {
            callback(groupId);
        });
    }

    this.removeDuplicatesFromOtherGroups = function (groups, groupId) {
        //remove duplicates - all admins from students
        var groupsAdminsIds = Helpers.toIds(groups.groups_admin);
        var are_archived = false;
        groups.groups_student = $filter('filter')(groups.groups_student, function (s) {
            are_archived |= s.is_archived;
            return s.id != groupId && groupsAdminsIds.indexOf(s.id) < 0;
        })
        groups.groups_admin = $filter('filter')(groups.groups_admin, function (s) {
            are_archived |= s.is_archived;
            return s.id != groupId;
        })
        groups.are_archived = are_archived;
        groups.count = groups.groups_student.length + groups.groups_admin.length;
        return groups;
    }
}

angular.module('myApp.services').service('groupService', ['$http', 'localStorageService', 'competitionService', '$filter', GroupService]);