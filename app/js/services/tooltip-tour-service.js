'use strict';

function TooltipTourService($translate) {

    this.getTooltipTourForLearn =
        function (callback) {
            $translate(
                ['TOUR_KEYBOARD', 'TOUR_CONFIGURATIONS_LEARN', 'TOUR_STATS', 'TOUR_INPUT', 'NEXT', 'PREVIOUS'])
                .then(function (t) {
                    var options = {
                        steps: [{
                            element: '#keyboard',
                            intro: t.TOUR_KEYBOARD,
                            position: 'top'
                        }, {
                            element: '#configurations',
                            intro: t.TOUR_CONFIGURATIONS_LEARN,
                            position: 'right'
                        }, {
                            element: '#stats',
                            intro: t.TOUR_STATS,
                            position: 'left'
                        }, {
                            element: '#text',
                            intro: t.TOUR_INPUT,
                            position: 'bottom'
                        }],
                        showStepNumbers: false,
                        exitOnOverlayClick: true,
                        exitOnEsc: true,
                        nextLabel: t.NEXT,
                        prevLabel: t.PREVIOUS,
                        skipLabel: 'x',
                        doneLabel: 'x'
                    };

                    callback(options);
                });
        }
    this.getTooltipTourForWrite =
        function (callback) {
            $translate(
                ['TOUR_KEYBOARD', 'TOUR_CONFIGURATIONS_WRITE', 'TOUR_STATS', 'TOUR_INPUT', 'NEXT', 'PREVIOUS'])
                .then(function (t) {
                    var options = {
                        steps: [{
                            element: '#keyboard',
                            intro: t.TOUR_KEYBOARD,
                            position: 'top'
                        }, {
                            element: '#configurations',
                            intro: t.TOUR_CONFIGURATIONS_WRITE,
                            position: 'right'
                        }, {
                            element: '#stats',
                            intro: t.TOUR_STATS,
                            position: 'left'
                        }, {
                            element: '#text',
                            intro: t.TOUR_INPUT,
                            position: 'bottom'
                        }],
                        showStepNumbers: false,
                        exitOnOverlayClick: true,
                        exitOnEsc: true,
                        nextLabel: t.NEXT,
                        prevLabel: t.PREVIOUS,
                        skipLabel: 'x',
                        doneLabel: 'x'
                    };

                    callback(options);
                });
        }

    this.getTooltipTourForCompetition =
        function (callback) {
            $translate(
                ['TOUR_POSITIONS', 'TOUR_CONFIGURATIONS_COMPETITION', 'TOUR_STATS', 'TOUR_INPUT', 'NEXT', 'PREVIOUS'])
                .then(function (t) {
                    var options = {
                        steps: [{
                            element: '#positionInCompetition',
                            intro: t.TOUR_POSITIONS,
                            position: 'top'
                        }, {
                            element: '#configurations',
                            intro: t.TOUR_CONFIGURATIONS_COMPETITION,
                            position: 'right'
                        }, {
                            element: '#stats',
                            intro: t.TOUR_STATS,
                            position: 'left'
                        }, {
                            element: '#text',
                            intro: t.TOUR_INPUT,
                            position: 'bottom'
                        }],
                        showStepNumbers: false,
                        exitOnOverlayClick: true,
                        exitOnEsc: true,
                        nextLabel: t.NEXT,
                        prevLabel: t.PREVIOUS,
                        skipLabel: 'x',
                        doneLabel: 'x'
                    };

                    callback(options);
                });
        }

}

angular.module('myApp.services').service('tooltipTourService', ['$translate', TooltipTourService]);