'use strict';

function TextsService($http, textsCalculationsService) {

    var letters;
    var words;
    var specials;

    this.loadLanguageDataFile = function (config, callback) {
        $http.get('data/' + config.lang + '.json').success(function (langData) {
            specials = textsCalculationsService.createSpecialChars(langData.specials);
            letters = langData.letters.concat(textsCalculationsService.getFullCharsFromSpecials(specials));
            words = langData.words;
            if (callback) {
                callback(config);
            }
        });
    }


    this.getForCompetition = function (config) {
        // TODO improve performance by appending if someone reaches this speed
        var limit = config.write_minutes * 300;
        return this.doGetForWrite(config, limit);
    }

    this.getLetters = function () {
        return letters;
    }

    this.getLettersNumber = function () {
        return letters.length;
    }

    var addFocusOnLetter = function (config, search) {
        search.terms.push({
            v: config.focus_letter,
            _text: true
        });
        return search;
    };


    this.getForLearn = function (config) {
        var lettersConfig = config.learn_config;
        var letterFrom = 6;//for reasonable words, 0 would also get the primitive ones
        var specialsLength = specials.length;
        var specialCharsEnabled = config.learn_config >= letters.length - specialsLength;
        //console.log(config.learn_config);
        //console.log(letters.length);
        //console.log(specialsLength);
        if (specialCharsEnabled) {
            //take the last non-special char
            lettersConfig = letters.length - 1 - specialsLength;
            //console.log(lettersConfig);
            //console.log("enabled special");
        } else {
            //special chars enabled so letterFrom is 6 - now focus on specials
            letterFrom = textsCalculationsService.retrieveLetterFromForLearn(lettersConfig, config);
        }
        var search = {
            terms: [{
                m: {
                    from: letterFrom,
                    to: lettersConfig
                }
            }]
        };
        // if at least letter nr 8 can put focus
        if (letterFrom > LAST_LETTER_INDEX_ALWAYS_INCLUDED && textsCalculationsService.shouldAddFocusForLearn(letters, config)) {
            search = addFocusOnLetter(config, search);
        }

        var results = SEARCHJS.matchArray(words, search);

        if (results.length === 0) {
            // clear focus letter because it could cause the problem like
            // multiple changes to configuration
            config.focus_letter = "";
            return this.getForLearn(config);
        }
        var limit = 15;// because they are normally shorter
        results = textsCalculationsService.shuffleArrayAndCut(results, limit);
        //if (false) {
        if (specialCharsEnabled) {
            var specialsIncluded = config.learn_config - (letters.length - specialsLength) + 1;
            var possibleSpecials = specials.slice(0, specialsIncluded);
            return textsCalculationsService.insertSpecialCharsBuildingString(results, possibleSpecials, config.focus_letter);
        }
        return buildStringFromArray(results);
    }

    this.getForWrite = function (config) {
        return this.doGetForWrite(config, 15);
    }

    this.doGetForWrite = function (config, limit) {
        var results = words;
        if (config.write_config < 6000 && config.write_config > 0) {
            var search = {
                terms: [{
                    p: {
                        from: 1000,
                        to: config.write_config
                    }
                }]
            };
            if (textsCalculationsService.shouldAddFocus(config)) {
                search = addFocusOnLetter(config, search);
            }

            results = SEARCHJS.matchArray(words, search);
        }
        results = textsCalculationsService.shuffleArrayAndCut(results, limit);
        return buildStringFromArray(results);
    }

    var buildStringFromArray = function (array) {
        var text = "";
        for (var i = 0; i < array.length; i++) {
            text += array[i].v + " ";
        }
        return text;
    }

    this.getForFallingLetters = function (config) {
        if (textsCalculationsService.shouldAddFocus(letters, config, true)) {
            return config.focus_letter;
        }
        var indexToGet = Helpers.getRandomInt(0, config.learn_config + 1);
        return letters[indexToGet];
    }
}

angular.module('myApp.services').service('textsService', ['$http', 'textsCalculationsService', TextsService]);