'use strict';

function LocalStorageService(xdLocalStorage) {

    this.put = function (key, value) {
        localStorage.setItem(key, value);
    }

    this.putObject = function (key, value) {
        this.put(key, JSON.stringify(value));
    }

    var putObjectToMainDomain = function (key, value, callback) {
        xdLocalStorage.setItem(key, JSON.stringify(value), callback);
    }

    this.putUserToMainDomain = function (value, callback) {
        putObjectToMainDomain("user", value, callback);
    }

    this.putUser = function (user) {
        this.putObject("user", user);
    }

    this.get = function (key, defaultValue) {
        var item = localStorage.getItem(key);
        if (item) {
            return item;
        }
        return defaultValue !== undefined ? defaultValue : null;
    }

    this.getInt = function (key, defaultValue) {
        var item = localStorage.getItem(key);
        if (item) {
            return parseInt(item);
        }
        return defaultValue !== undefined ? defaultValue : null;
    }

    this.getObject = function (key) {
        return JSON.parse(localStorage.getItem(key));
    }

    this.getUser = function () {
        return this.getObject("user");
    }

    this.getToken = function () {
        var user = this.getObject("user");
        if (user) {
            return user.token;
        }
        return null;
    }

    this.remove = function (key) {
        localStorage.removeItem(key);
    }

    this.shouldDisableAdblock = function () {
        //should show the message if at least 3 tests taken
        var testsTaken = this.getInt('testsTaken', 0);
        return testsTaken > 3;
    }

    this.storeTestTaken = function () {
        var testsTaken = this.getInt('testsTaken', 0);
        testsTaken++;
        this.put('testsTaken', testsTaken);
    }

}

angular.module('myApp.services')
    .service('localStorageService', ['xdLocalStorage', LocalStorageService]);