'use strict';


function CompetitionService($http, textsService) {

    this.getHistory = function (userId, callback) {
        $http.get(REST + 'competition/getUserHistory/' + userId).success(function (h) {
            callback(h);
        });
    }

    this.getPositionsHistory = function (userId, callback) {
        $http.get(REST + 'competition/getUserPositionsHistory/' + userId).success(function (h) {
            callback(h);
        });
    }

    this.saveCompetition = function (competition, callback) {
        var config = {
            write_minutes: competition.typing_minutes,
            write_config: competition.typing_words
        };
        competition.text = textsService.getForCompetition(config);
        $http.post(REST + 'competition/save', competition).success(function (id) {
            callback(id);
        });
    }
    this.getCompetition = function (id, key, callback) {
        $http.get(REST + 'competition/getIfActive/' + id + "/" + key).success(function (competition) {
            callback(competition);
        });
    }

    this.getPositionsOfCompetition = function (id, callback) {
        $http.get(REST + 'competition/getPositionsOfCompetition/' + id).success(function (competition) {
            callback(competition);
        });
    }

    this.getPositionsOfCompetitionsInGroup = function (groupId, callback) {
        $http.get(REST + 'competition/getPositionsOfCompetitionsInGroup/' + groupId).success(function (competitions) {
            callback(competitions);
        });
    }

    this.getAll = function (callback) {
        $http.get(REST + 'competition/all/').success(function (competitions) {
            callback(competitions);
        });
    }

    this.showInitialPositions = function (initialPositions, howManyToShow) {
        var positions = [];
        for (var i = 0; i < initialPositions.length; i++) {
            positions.push(initialPositions[i]);
            positions[i].no = i + 1;
        }
        var added = {
            positions: positions,
            index: 0
        };
        return this.sliceCollectionToShowGivenUser(added, howManyToShow);
    }

    this.calculatePositions = function (initialPositions, user, result, howManyToShow) {
        var currentApproach = {
            id: user.id,
            nick: user.nick,
            picture: user.picture,
            best_result: result,
            type: 'current'
        };

        var added = this.putCurrentApproachAmongPositions(initialPositions, currentApproach)
        return this.sliceCollectionToShowGivenUser(added, howManyToShow);
    }

    this.sliceCollectionToShowGivenUser = function (added, howManyToShow) {
        var oddFactor = howManyToShow % 2;
        if (added.positions.length <= howManyToShow) {
            return added.positions;
        } else {
            var start = added.index;
            var end = added.index;
            while (end - start < howManyToShow - oddFactor) {
                var before = start - 1;
                if (before >= 0) {
                    start = before;
                }
                var after = end + 1;
                if (after < added.positions.length) {
                    end = after;
                }
            }
            return added.positions.slice(start, end + 1);
        }
    }

    this.putCurrentApproachAmongPositions = function (initialPositions, currentApproach) {
        var positions = [];
        var isInserted = 0;
        var foundAtIndex = -1;
        var sameWorse = 0;
        for (var i = 0; i < initialPositions.length; i++) {
            var current = initialPositions[i];
            if (foundAtIndex < 0 && current.best_result < currentApproach.best_result) {
                foundAtIndex = i;
                isInserted = 1;
                if (currentApproach.type == 'worseTry') {
                    currentApproach.no = '-';
                    sameWorse = -1;
                } else {
                    currentApproach.no = i + 1;
                }
                positions.push(currentApproach);
            }

            // position can be overwritten yet
            current.no = i + 1 + isInserted + sameWorse;
            if (current.id == currentApproach.id) {
                if (isInserted) {
                    current.type = 'sameWorse';
                    current.no = '-';
                    sameWorse = -1;
                } else {
                    current.type = 'same';
                    currentApproach.type = 'worseTry';
                }
            }
            positions.push(current);
        }
        if (!isInserted) {
            foundAtIndex = initialPositions.length;
            if (currentApproach.type == 'worseTry') {
                currentApproach.no = '-';
            } else {
                currentApproach.no = initialPositions.length + 1;
            }
            positions.push(currentApproach)
        }
        return {
            positions: positions,
            index: foundAtIndex
        };
    }
}

angular.module('myApp.services').service('competitionService', ['$http', 'textsService', CompetitionService]);