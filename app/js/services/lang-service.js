'use strict';

/* To retrieve the current language, translate and moment locale are set here - the language version shown on the page
 * is taken from the user - if he is not logged in the the language retrieved here is used
 */
function LangService($window, $translate, $moment) {

    var validateLang = function (inputLang) {
        if (inputLang) {
            // sometimes it's en_US, so we take only en
            var lang = inputLang.substring(0, 2);
            if (lang in TRANSLATION_LANGUAGES) {
                return lang;
            }
        }
        return null;
    }

    this.getDomainLang = function () {
        var lang = $window.location.host.substr(0, $window.location.host.indexOf('.'));
        return validateLang(lang);
    }

    var getNavigatorLang = function () {
        var lang = $window.navigator.userLanguage || $window.navigator.language;
        return validateLang(lang);
    }

    this.getSiteLang = function () {
        var lang = this.getDomainLang();//1 sub domain
        if (lang) {
            return lang;
        }
        lang = getNavigatorLang();//2 navigator
        if (lang) {
            return lang;
        }
        return 'en';//3 default english
    }

    this.changeSiteLang = function (lang) {
        $translate.use(lang);
        $moment.locale(lang);
    }

}

angular.module('myApp.services').service('langService', ['$window', '$translate', '$moment', LangService]);