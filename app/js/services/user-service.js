'use strict';

function UserService($http, typingConfigService, localStorageService, langService, $timeout) {

    var loginChangeCallback;
    var showGroupInMenuCallback;

    this.registerLoginChangeCallback = function (callback) {
        loginChangeCallback = callback;
    }

    this.registerGroupAddedCallback = function (callback) {
        showGroupInMenuCallback = callback;
    }

    this.showGroupInMenu = function () {
        showGroupInMenuCallback();
    }

    var getDefaultUser = function () {
        return {
            id: 0,
            email: null,
            site_lang: langService.getSiteLang(),
            picture: REST + 'application/img/avatars/default.jpg',
            last_group: null //not logged user never has a group
        };
    }

    var retrieveCurrentUser = function () {
        var user = localStorageService.getUser();
        if (user) {
            return user;
        } else {
            return getDefaultUser();
        }
    }
    var currentUser = retrieveCurrentUser();

    this.getSiteLang = function () {
        return currentUser.site_lang;
    }

    this.getCurrentUser = function () {
        return currentUser;
    }

    this.isCurrentUserLoggedIn = function () {
        return currentUser != null && currentUser.id > 0;
    }

    function doAfterLogin(user, callback) {
        if (user.id > 0) {
            //the error could be parsed, then this shouldn't be stored
            currentUser = user;
            typingConfigService.clean();
            localStorageService.putUserToMainDomain(user, callback);
        }
    }

    this.login = function (userToLogin, callback) {
        $http.post(REST + 'user/login', userToLogin).success(function (user) {
            doAfterLogin(user, callback);
        });
    }

    this.sendEmailForPersona = function (callback, email) {
        $http.post(REST + 'user/login', {
            site_lang: currentUser.site_lang,
            provider: 'persona',
            provider_id: '',//leave empty but thave it as it will be calculated on the backend
            email: email,
            base_url: Helpers.getMainDomainWithCurrentLang() + Helpers.getRoute('login', currentUser.site_lang)
        }).success(function (sent) {
            callback(sent);
        });
    }

    /**
     * Need to wait because it get's initialized in the lazy way
     * normally it should always be ok at this time
     * because the backend logic before should take some time
     */
    var waitForLocalStorageAndDoAfterLogin = function (user, callback) {
        if (!localStorageReady) {
            $timeout(function () {
                waitForLocalStorageAndDoAfterLogin(user, callback);
            }, 50);
        } else {
            doAfterLogin(user, callback);
        }
    }

    this.loginWithPersona = function (code, email, callback) {
        $http.post(REST + 'user/loginByEmail', {
            provider_id: code,
            email: email,
            provider: 'persona',
            site_lang: currentUser.site_lang
        }).success(function (user) {
            waitForLocalStorageAndDoAfterLogin(user, callback);
        });
    }

    this.logout = function (callback) {
        localStorageService.remove("user");
        //preserve the language, also on refresh, so default is saved as current
        var lang = currentUser.site_lang;
        currentUser = getDefaultUser();
        currentUser.site_lang = lang;
        //it always logs out to main domain, so no need for iFrame
        localStorageService.putObject("user", currentUser);
        loginChangeCallback();
        if (callback) {
            callback();
        }
    }

    this.getRoute = function (route) {
        return Helpers.getRoute(route, currentUser.site_lang);
    }

    this.changeSiteLang = function (newLang) {
        currentUser.site_lang = newLang;
        localStorageService.putUserToMainDomain(currentUser);
        langService.changeSiteLang(newLang);
        if (this.isCurrentUserLoggedIn()) {
            $http.post(REST + 'user/changeSiteLang/' + currentUser.id, {
                value: newLang
            });
        }
    }

    this.getById = function (userId, callback) {
        $http.get(REST + 'user/get/' + userId).success(function (c) {
            callback(c);
        });
    }

    this.changeNick = function (nick) {
        $http.post(REST + 'user/changeNick/' + currentUser.id, {
            value: nick
        }).success(function () {
            currentUser.nick = nick;
            currentUser.can_change_nick = false;
            localStorageService.putUser(currentUser);
        });
    }

    this.changePicture = function (picture) {
        $http.post(REST + 'user/changePicture/' + currentUser.id, {
            dataUrl: picture.resized.dataURL,
            name: picture.file.name
        }).success(function (url) {
            currentUser.picture = url;
            localStorageService.putUser(currentUser);
        });
    }

}

angular.module('myApp.services').service('userService', ['$http', 'typingConfigService', 'localStorageService', 'langService', '$timeout', UserService]);