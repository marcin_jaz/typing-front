'use strict';

function ErrorService($sce, $injector, $timeout, localStorageService) {
    var observerCallback;
    var messages = new Array();
    var timer;

    var hideMessagesTillRefresh = new Array();

    this.registerObserverCallback = function (callback) {
        observerCallback = callback;
    };

    // warning added on timeout so safer to check to avoid duuplication than
    // cancel timeout on route change
    var alreadyContains = function (msg) {
        for (var i = 0; i < messages.length; i++) {
            if (messages[i].id === msg) {
                return true;
            }
        }
        return false;
    }

    this.hideMessage = function (id) {
        if (MSG_CLEAR_REFRESH.indexOf(id) >= 0) {
            hideMessagesTillRefresh.push(id);
        } else if (MSG_CLEAR_COOKIE.indexOf(id) >= 0) {
            localStorageService.put(id, true);
        }
    }

    var isHidden = function (msg) {
        if (MSG_CLEAR_REFRESH.indexOf(msg) >= 0) {
            return hideMessagesTillRefresh.indexOf(msg) >= 0;
        }
        var v = localStorageService.get(msg);
        return v === true;
    }

    var setErrorInternally = function (error, ttl) {
        if (error.shouldTranslate !== false) {
            // if it wasn't closed
            if (!isHidden(error.message)) {
                // due to circular dependency must be injected here
                var $translate = $injector.get("$translate");
                if (!alreadyContains(error.message)) {
                    timer = $timeout(function () {
                        $translate(error.message, error.message_params).then(function (translated) {
                            var m = new Message($sce.trustAsHtml(translated), error.type, ttl);
                            m.id = error.message;
                            messages.push(m);
                            observerCallback(messages);
                        });
                    }, 100);
                }
            }
        } else if (!alreadyContains(error.message)) {
            var m = new Message($sce.trustAsHtml(error.message), error.type, ttl)
            messages.push(m);
            observerCallback(messages);
        }
    }

    this.setError = function (error) {
        setErrorInternally(error, 0);
    }

    this.setErrorWithRedirection = function (error) {
        // set to two because of redirection
        setErrorInternally(error, 2);
    }

    this.cleanError = function () {
        if (timer) {
            $timeout.cancel(timer);
        }
        for (var i = 0; i < messages.length; i++) {
            var m = messages[i];
            if (m.shouldCleanMessage()) {
                messages.splice(i, 1);
                i--;
            }
        }
        observerCallback(messages);
    }

    this.messageSuccess = function (key, params) {
        var info = {
            message: key,
            type: SUCCESS,
            message_params: params
        };
        this.setError(info);
    }

    this.messageError = function (key) {
        var info = {
            message: key,
            type: ERROR
        };
        this.setError(info);
    }

    this.messageWarning = function (key) {
        var info = {
            message: key,
            type: WARNING
        };
        this.setError(info);
    }

    this.messageNotLoggedIn = function () {
        this.messageWarning('NOT_LOGGED_IN_MSG');
    }

    this.showDisableAdblockMessageIfNeeded = function () {
        var pThis = this;
        if (window.ADS_ENABLED === undefined && localStorageService.shouldDisableAdblock()) {
            timer = $timeout(function () {
                $('#write').addClass('typing-well');
                $('#writeInput').attr('disabled', 'disabled');

                messages.splice(0, messages.length);//just clear all others to make adblock msg visible
                pThis.messageError('DISABLE_ADBLOCK');
            }, 400);
        }
    }

}

angular.module('myApp.services').service('errorService',
    ['$sce', '$injector', '$timeout', 'localStorageService', ErrorService]);