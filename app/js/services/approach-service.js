'use strict';

function ApproachService($http, textsService, localStorageService) {

    this.storeTestTaken = function () {
        localStorageService.storeTestTaken();
    }

    this.saveWriteApproach = function (userId, config, stats) {
        if (userId > 0) {
            var approach = new Approach(userId, WRITE, config, stats);
            this.saveApproach(approach);
        }
        this.storeTestTaken();
    }

    this.saveLearnApproach = function (userId, config, stats) {
        if (userId > 0) {
            var approach = new Approach(userId, LEARN, config, stats);
            this.saveApproach(approach);
        }
        this.storeTestTaken();
    }

    this.saveCompetitionApproach = function (userId, competition, stats) {
        if (userId > 0) {
            // config as for write
            var config = {
                lang: competition.lang,
                type: COMPETITION,
                write_minutes: competition.typing_minutes,
                write_config: competition.typing_words
            };

            var approach = new Approach(userId, COMPETITION, config, stats);
            approach.competition_id = competition.id;
            this.saveApproach(approach);
        }
        this.storeTestTaken();
    }

    this.saveApproach = function (approach) {
        $http.post(REST + 'approach/save', approach);
    }

    this.getAllForModes = function (userId, modes, callback) {
        $http.get(REST + 'approach/all/' + userId, {
            params: {
                modes: modes
            }
        }).success(function (approaches) {
            callback(approaches);
        });
    }
    this.getSummaryForModes = function (userId, modes, callback) {
        $http.get(REST + 'approach/summary/' + userId, {
            params: {
                modes: modes
            }
        }).success(function (summary) {
            callback(summary);
        });
    }

    this.getResultFactorForWrite = function (words) {
        if (words > 5000) {
            return 1;
        } else {
            return (words / 20000) + 0.7;
        }
    }

    this.getResultFactorForLearn = function (letters) {
        return (letters + 1) / textsService.getLettersNumber();
    }
}

angular.module('myApp.services').service('approachService', ['$http', 'textsService', 'localStorageService', ApproachService]);