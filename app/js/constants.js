var REST = window.location.protocol + '//' + window.location.host + '/pisanie/';
var HOME = 'typing1';

var MSG_CLEAR_REFRESH = new Array('NOT_LOGGED_IN_MSG', 'LEARN_INFO', 'WRITE_INFO');
var MSG_CLEAR_COOKIE = new Array();// new Array('LEARN_INFO', 'WRITE_INFO');
var SHOULD_OMIT_NO_LOGIN_MESSAGE = ['index', 'login', 'benefits', 'howtolearn', 'school', 'group'];

var ERROR = "danger";
var WARNING = "warning";
var SUCCESS = "success";
var INFO = "info";

var WRITE = 'write';
var LEARN = 'learn';
var COMPETITION = 'competition';

var ERROR_WRONG = 5000;
var ERROR_MISSED = 3000;
var ERROR_EXCESSIVE = 2000;
// when points for letter more that it can be under focus
var ERROR_TO_BE_FOCUSED_ON = 999;
// after each rest call points will be divided by this number
var ERROR_DIVIDER_TO_DISSAPEAR = 6;

// for automatic configuration ajustment when learning
var ACCURACY_MIN_FOR_GOOD = 94;
var RESULT_MIN_FOR_GOOD = 28;// does not include config difficulty
// has no effect now, as with ACCURACY_MIN_FOR_GOOD it will be stopped
var ACCURACY_MAX_FOR_BAD = 90;
var RESULT_MAX_FOR_BAD = 15;

var KEYSTROKES_IN_WORD = 5;

var COMPETITION_CLASSIFICATION_USERS = 5;// how many competitions to display

var LAST_LETTER_INDEX_ALWAYS_INCLUDED = 3;// so 4 first letters in fact

var SIGN_TRUE = '\u2713';
var SIGN_FALSE = '\u2718';
var ARROW_DOWN = '\u25B2';
var ARROW_UP = '\u25BC';

var KEY_ENTER = 13;

// de : 'Deutsch',
// hr : 'Hrvatski',
var TRANSLATION_LANGUAGES = {
    en: 'English',
    es: 'Español',
    pl: 'Polski'
};

var WRITING_LANGUAGES = {
    de: 'Deutsch',
    en: 'English',
    es: 'Español',
    fr: 'Français',
    hr: 'Hrvatski',
    it: 'Italiano',
    pl: 'Polski',
    ru: 'Русский'
};
// pt: 'Português'
