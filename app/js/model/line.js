
function Line(text, resultFactor) {
    this.text = text;
    this.nextExpected = text.charAt(0);
    this.textToShow = ' ';
    this.length = text.length;
    this.keystrokesTotal = this.countKeystrokes(text);

    this.writtenText = '';
    this.stats = new Stats(resultFactor);
    this.isFinished = false;
    this.keysStats = {};

    this.remainingChars = 100;

    this.textTodo = text;
}

Line.prototype.ajustResultFactor = function (newResultFactor) {
    return this.stats.ajustResultFactor(newResultFactor);
}

Line.prototype.countKeystrokes = function (text) {
    var keystrokes = 0;
    for (var i = 0; i < text.length; i++) {
        var letter = text.charAt(i);
        if (letter == ' ') {
            keystrokes++;
        } else if (DOUBLE_KEYSTROKES.indexOf(letter.toLowerCase()) >= 0) {
            if (this.isUpperCase(letter)) {
                keystrokes += 3;
            } else {
                keystrokes += 2;
            }
        } else if (this.isUpperCase(letter)) {
            keystrokes += 2;
        } else {
            keystrokes++;
        }
    }
    return keystrokes;
};

Line.prototype.isUpperCase = function (letter) {
    return letter.toUpperCase() == letter;
};

Line.prototype.addToKeyStats = function (letter, error) {
    if (letter in this.keysStats) {
        this.keysStats[letter] += error;
    } else {
        this.keysStats[letter] = error;
    }
}

Line.prototype.getLetterToPutFocusOn = function () {
    var max = 0;
    var letter = "";
    for (index in this.keysStats) {
        var v = this.keysStats[index];
        if (v > max) {
            max = v;
            letter = index;
        }
    }
    if (max > 999) {
        delete this.keysStats[letter];// it will be focused so can be removed
        return letter;
    }
    return "";// must be double for json!
}

Line.prototype.fuzzyErrors = function () {
    for (index in this.keysStats) {
        var v = this.keysStats[index];
        this.keysStats[index] = v / ERROR_DIVIDER_TO_DISSAPEAR;
    }
}

Line.prototype.updateStatsOnChange = function () {
    this.keysStats = {};
    this.textToShow = '';
    var errors = 0;
    var shift = 0;
    var lastWasError = false;
    if (this.writtenText.length > 0) {
        if (this.writtenText.charAt(0) != this.text.charAt(0)) {
            if (this.writtenText.charAt(0) == this.text.charAt(1)) {
                // missed
                shift++;
                lastWasError = true;
                this.addToKeyStats(this.text.charAt(0), ERROR_MISSED);
                this.showError(this.text.charAt(0));
                this.showCorrect();
            } else if (this.writtenText.charAt(1) == this.text.charAt(0)) {
                // excessive
                this.addToKeyStats(this.writtenText.charAt(0), ERROR_EXCESSIVE);
                // this.showCorrect();
                this.showError(this.text.charAt(0));
            } else {
                // wrong
                this.addToKeyStats(this.writtenText.charAt(0), ERROR_EXCESSIVE);
                this.addToKeyStats(this.text.charAt(0), ERROR_WRONG);
                this.showError(this.text.charAt(0));
            }
            errors++;
        } else {
            this.showCorrect();
        }
        for (var i = 1; i < this.writtenText.length; i++) {
            var written = this.writtenText.charAt(i);
            var shifted = i + shift;
            if (this.isShiftedInBoundsOfText(shifted)) {
                errors++;
                lastWasError = true;
                this.showError(expected);
                continue;
            }
            var expected = this.text.charAt(shifted);
            if (written != expected) {
                // missed
                if (shifted + 1 < this.length && written == this.text.charAt(shifted + 1)) {
                    shift++;
                    errors++;
                    lastWasError = true;
                    this.addToKeyStats(expected, ERROR_MISSED);
                    this.showError(expected);
                    this.showCorrect();
                } else
                // excessive
                if (lastWasError && shifted - 1 >= 0 && written == this.text.charAt(shifted - 1)) {
                    shift--;
                    lastWasError = false;
                    // subtract because in last it was qualified as error
                    this.addToKeyStats(written, -ERROR_WRONG);
                    this.addToKeyStats(expected, ERROR_EXCESSIVE);
                    this.addToKeyStats(written, ERROR_EXCESSIVE);
                }// wrong
                else {
                    errors++;
                    lastWasError = true;
                    this.addToKeyStats(written, ERROR_EXCESSIVE);
                    this.addToKeyStats(expected, ERROR_WRONG);
                    this.showError(expected);
                }

            } else {
                lastWasError = false;
                this.showCorrect();
            }
        }
    }
    this.nextExpected = this.text.charAt(this.writtenText.length + shift);
    this.textTodo = this.text.substr(this.writtenText.length + shift);
    this.textDone = this.text.substr(0, this.writtenText.length + shift);
    this.isFinished = this.writtenText.length + shift + 1 >= this.length;
    // must be before updating errors
    if (this.stats.errors > errors) {
        this.stats.corrections += this.stats.errors - errors;
    }
    this.stats.errors = errors;
    this.stats.keystrokesExpected = this.countKeystrokes(this.text.slice(0, this.writtenText.length + shift));
    this.stats.keystrokes = this.countKeystrokes(this.writtenText);
    this.stats.calculateAccuracy();

    this.remainingChars = this.text.length - this.writtenText.length + shift;

    if (this.textToShow == '') {
        this.textToShow = ' ';
    }
    return this.stats;
};

Line.prototype.showError = function (letter) {
    if (letter == ' ') {
        this.textToShow += '_';
    } else {
        this.textToShow += letter;
    }
}
Line.prototype.showCorrect = function () {
    this.textToShow += ' ';
}

Line.prototype.isShiftedInBoundsOfText = function (shifted) {
    return shifted < 0 || shifted >= this.length;
};

Line.prototype.updateStatsWithTime = function () {
    this.stats.updateWithTime();
};

Line.prototype.hasFinished = function () {
    return this.stats.timeLeft <= 0;
};

Line.prototype.shouldAppendText = function () {
    return this.remainingChars < 44;
};

Line.prototype.appendText = function (v) {
    this.length += v.length;
    this.keystrokesTotal += this.countKeystrokes(v);
    this.text += v;
};

Line.prototype.isGoodPerformance = function () {
    return this.stats.isGoodPerformance();
};

Line.prototype.isBadPerformance = function () {
    return this.stats.isBadPerformance();
};

Line.prototype.isBadAccuracy = function () {
    return this.stats.isBadAccuracy();
};
