function Approach(userId, type, config, stats) {
    this.userId = userId;

    this.lang = config.lang;
    this.type = type;// ENUM('write','learn','competition');

    if (type == LEARN) {
        this.typingMinutes = config.learn_minutes;
        this.config = config.learn_config;
    } else {
        this.typingMinutes = config.write_minutes;
        this.config = config.write_config;
    }

    this.wpm = stats.wpm;
    this.accuracy = stats.accuracy;
    this.keystrokes = stats.keystrokes;
    this.result = stats.result;
    this.result_factor = stats.result_factor;
}
