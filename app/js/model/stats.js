function Stats(resultFactor) {
    this.keystrokes = 0;
    this.errors = 0;
    this.corrections = 0;
    this.keystrokesExpected = 0;

    this.time = 0;
    this.wpm = (0).toFixed(2);
    this.accuracy = (0).toFixed(2);
    this.progress = 0;
    // by default 1 minute, gets set in controller explicitly
    this.timeToWrite = 60;
    this.timeLeft = this.timeToWrite;
    this.resultFactor = resultFactor;
    this.result = (0).toFixed(2);
    this.minResultForGood = RESULT_MIN_FOR_GOOD;
}

Stats.prototype.ajustResultFactor = function (newResultFactor) {
    var oldFactor = this.resultFactor;
    this.resultFactor = ((this.resultFactor + newResultFactor) / 2);
    return oldFactor != this.resultFactor;
}

Stats.prototype.calculateAccuracy = function () {
    var expected = this.keystrokesExpected + this.corrections;
    if (expected == 0) {
        this.accuracy = 0;
        return;
    }
    var a = (this.keystrokesExpected - this.errors) / expected;
    a = (a * 100).toFixed(2);
    if (a < 0) {
        this.accuracy = (0).toFixed(2);
    } else {
        this.accuracy = a;
    }
};

Stats.prototype.updateWithTime = function () {
    this.time++;
    // For the purpose of typing measurement, each word is standardized to be
    // five characters or keystrokes long
    this.wpm = ((this.keystrokes / KEYSTROKES_IN_WORD) * 60 / this.time).toFixed(2);
    this.progress = (this.time / this.timeToWrite) * 100;
    this.timeLeft = this.timeToWrite - this.time;
    this.result = ((this.wpm * this.accuracy * this.resultFactor) / 100).toFixed(2);
};

Stats.prototype.getResult = function () {
    return (this.wpm * this.accuracy) / 100;
}

Stats.prototype.isGoodPerformance = function () {
    if (this.accuracy < ACCURACY_MIN_FOR_GOOD) {
        return false;
    }
    var result = (this.wpm * this.accuracy) / 100;
    var r = this.getResult() >= this.minResultForGood;
    if (r === true) {
        this.minResultForGood = result * 0.93;
        if (this.minResultForGood < RESULT_MIN_FOR_GOOD) {
            this.minResultForGood = RESULT_MIN_FOR_GOOD;
        }
    }
    return r;
}

Stats.prototype.isBadPerformance = function () {
    if (this.accuracy < ACCURACY_MAX_FOR_BAD) {
        return true;
    }
    var result = (this.wpm * this.accuracy) / 100;
    return result < RESULT_MAX_FOR_BAD;
}

Stats.prototype.isBadAccuracy = function () {
    return this.keystrokes > 20 && this.accuracy < ACCURACY_MAX_FOR_BAD;
}
