function Message(text, type, ttl) {
    this.text = text;
    this.type = type;
    this.ttl = ttl;
}

Message.prototype.shouldCleanMessage = function () {
    this.ttl--;
    return this.ttl <= 0;
}

Message.prototype.equalTo = function (text) {
    return this.text === text;
}