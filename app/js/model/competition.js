function Competition(config, userId) {
    this.founder_user_id = userId;

    this.typing_minutes = config.write_minutes;
    this.typing_words = config.write_config;
    this.lang = config.lang;
    if (config.private_competition) {
        this.private_key = Helpers.createPrivateKey();
    } else {
        this.private_key = "";
    }
    this.group_id = 'NULL';//for the backend null literally should be sent, otherwise foreign key falls
}

Competition.prototype.isPrivate = function () {
    return this.private_key.length > 0;
};
