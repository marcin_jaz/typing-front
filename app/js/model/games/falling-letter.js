var INITIAL_Y = -39;

function FallingLetter(value, x, id) {
    this.id = id;// to remove it
    this.x = x;
    this.y = INITIAL_Y;
    this.value = value;
}

FallingLetter.prototype.updateYifPossible = function (y) {
    if (this.y === INITIAL_Y) {
        this.y = y;
    }
}

FallingLetter.prototype.canBeRemoved = function (pressed) {
    return this.y > INITIAL_Y && this.value === pressed;
}
