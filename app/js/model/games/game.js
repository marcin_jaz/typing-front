//Adapter for line so that we can use it's mechanism for calculations, but the text is different, 
//as we know about errors in controller and we don't need detailed calculations like for text
function Game(resultFactor) {
    this.line = new Line('', resultFactor);
    this.onSecondCounter = 1;
    this.level = 1;
}

// when it fails to the bottom
Game.prototype.addMissedError = function (letter) {
    this.line.addToKeyStats(letter, ERROR_MISSED);
    this.line.stats.errors += 2;
    var keystrokes = this.line.countKeystrokes(letter);
    this.line.stats.keystrokesExpected += 2 * keystrokes;
    this.line.stats.calculateAccuracy();
};

// when nothing was removed despite sth was typed
Game.prototype.addExcessiveError = function (letter) {
    this.line.addToKeyStats(letter, ERROR_EXCESSIVE);
    this.line.stats.errors++;
    this.line.stats.calculateAccuracy();
};

Game.prototype.letterTyped = function (letter) {
    var keystrokes = this.line.countKeystrokes(letter);
    this.line.stats.keystrokesExpected += keystrokes;
    this.line.stats.keystrokes += keystrokes;
    this.line.stats.calculateAccuracy();
};

Game.prototype.updateWithTime = function () {
    // because event fired every 500ms and we count seconds
    this.onSecondCounter = -this.onSecondCounter;
    if (this.onSecondCounter > 0) {
        this.line.updateStatsWithTime();
    }
}

Game.prototype.getLetterToPutFocusOn = function () {
    return this.line.getLetterToPutFocusOn();
}

Game.prototype.updateLevel = function () {
    // accuracy is in percent
    var resultFactor = this.line.stats.wpm * this.line.stats.accuracy / 1000;
    var timeFactor = (this.line.stats.keystrokes - this.line.stats.errors * 2) / 10;
    var newLevel = Math.floor(timeFactor + resultFactor) + 1;
    this.level = newLevel < 1 ? 1 : newLevel;
}

Game.prototype.getSpeed = function () {
    return this.line.stats.wpm * 12 + this.level * 20;
}

Game.prototype.setTimeToWrite = function (minutes) {
    return this.line.stats.timeToWrite = minutes * 60;
}

Game.prototype.hasFinished = function () {
    return this.line.hasFinished();
}
