function SpecialChar(content) {
    this.content = content.v;
    this.nextToUpper = content.r.charAt(0) === '1';//0 or 1
    var shouldNotCloseWith = content.r.charAt(1) === '0';//0 not close
    if (shouldNotCloseWith) {
        this.closeWith = '';
    } else {
        this.closeWith = content.r.charAt(1);
    }
    this.spaceBefore = content.r.charAt(2) === '1';
    this.spaceAfter = content.r.charAt(3) === '1';
}

SpecialChar.prototype.getTextToAppend = function () {
    var text = "";
    if (this.spaceBefore) {
        text = " ";
    }
    text += this.content.charAt(0);
    if (this.spaceAfter) {
        text += " ";
    }
    return text;
}

SpecialChar.prototype.shouldNextBeUppercase = function () {
    return this.nextToUpper;
}