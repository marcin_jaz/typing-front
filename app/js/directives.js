/* Directives */
angular.module('myApp.directives', [])
    .directive('writing', function () {
        return {
            restrict: 'E',
            templateUrl: "./partials/directives/write-input.html",
            scope: true,

            link: function (scope) {
                scope.focusWrite = function () {
                    $('#writeInput').focus();
                }
            }

        };
    })
    //typing summary
    .directive('typingSummary', function () {
        return {
            restrict: 'E',
            templateUrl: "./partials/directives/write-summary.html",
            scope: true
        };
    })
    .directive('keyboard', function () {
        return {
            restrict: 'E',
            templateUrl: "./partials/directives/keyboard.html",
            scope: {
                key: "@key",
                lang: "@lang"
            },

            link: function (scope, element, attrs) {
                var fingers = [];
                attrs.$observe('key', function (key) {
                    var keySpecialForLang = key + "|" + scope.lang;
                    if (keySpecialForLang in KEYBOARD) {
                        fingers = KEYBOARD[keySpecialForLang];
                    } else {
                        fingers = KEYBOARD[key];
                    }
                    // for shift - currently not suported
                    // if (key.toLowerCase() != key) {
                    // if (scope.fingers[0].substr(0, 1) == 'l') {
                    // scope.fingers.push(R5);
                    // } else {
                    // scope.fingers.push(L5);
                    // }
                    // }
                });
                scope.is = function (finger) {
                    if (fingers != undefined) {
                        return fingers.indexOf(finger) >= 0;
                    }
                    return false;
                };
            }

        };
    })
    // this works only for single value, not an object!
    .directive('positions', function () {
        return {
            restrict: 'E',
            templateUrl: "./partials/directives/competition-positions.html",
            scope: true
        };
    })

    // ads
    .directive('aaddss', function () {
        return {
            restrict: 'E',
            template: '<ng-include src="template"/>',
            link: function (scope) {
                const path = './partials/directives/aaddss/';
                scope.template = path + "elgoog-da-size.html";
            }
        }
    })
    .directive('googleadelastic', function () {
        return {
            restrict: 'E',
            templateUrl: "./partials/directives/aaddss/elgoog-da-elastic.html"
        };
    })

    //group students
    .directive('groupstudents', function () {
        return {
            restrict: 'E',
            templateUrl: "./partials/directives/group-students.html",
            scope: true
        };
    })
    //group competitions
    .directive('groupcompetitions', function () {
        return {
            restrict: 'E',
            templateUrl: "./partials/directives/group-competitions.html",
            scope: true
        };
    })
;
