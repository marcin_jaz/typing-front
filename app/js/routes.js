var mainTitle = {
    pl: 'TouchTyping.guru - szybkie pisanie bezwzrokowe',
    en: 'TouchTyping.guru - improve typing speed',
    es: 'TouchTyping.guru - mecanografía al tacto'
};
var mainKeywords = {
    pl: 'pisanie bezwzrokowe, szybkie pisanie, kurs szybkiego pisania, kurs pisania bezwzrokowego, mistrz klawiatury',
    es: 'mecanografia, lecciones de mecanografía, teclar rápido sin mirar, mecanografía con 10 dedos, mecanografía online',
    en: 'touch typing, improve typing speed, touch typing course, touch typing tutor, typing master'
};

var ROUTES = {

    index: {
        val: {
            templateUrl: 'partials/main.html',
            controller: 'MainController',
            resolve: initializeUser
        },
        params: '',
        langs: {
            en: '',
            es: '',
            pl: ''
        },
        title: mainTitle,
        keywords: mainKeywords
    },

    learn: {
        val: {
            templateUrl: 'partials/learn.html',
            controller: 'LearnController',
            resolve: initializeUser
        },
        params: '',
        langs: {
            en: 'learn-touch-typing',
            es: 'aprender-mecanografia',
            pl: 'nauka-szybkiego-pisania'
        },
        title: {
            pl: 'Nauka szybkiego pisania - kurs pisania bezwzrokowego online',
            es: 'Aumenta tu velocidad al teclado - curso de mecanografía online ',
            en: 'Improve your typing speed - touch typing course online'
        },
        keywords: {
            pl: 'pisanie bezwzrokowe, szybkie pisanie, nauka szybkiego pisania na klawiaturze, kurs pisania bezwzrokowego online',
            es: 'mecanografía, aprender teclar rápido, curso de mecanografía online, mecanografía con 10 dedos online',
            en: 'touch typing, quick typing, learn keyboard typing, learn touch typing, touch typing course online'
        }
    },

    write: {
        val: {
            templateUrl: 'partials/write.html',
            controller: 'WriteController',
            resolve: initializeUser
        },
        params: '',
        langs: {
            en: 'test-typing-speed',
            es: 'test-velocidad-al-teclado',
            pl: 'test-szybkosci-pisania'
        },
        title: {
            pl: 'Test szybkości pisania',
            es: 'Test velocidad al teclado',
            en: 'Test typing speed'
        },
        keywords: {
            pl: 'pisanie bezwzrokowe, szybkie pisanie, test szybkosci pisania, test prędkosci pisania',
            es: 'tipear sin mirar, teclar rápido, test de velocidad al teclado, test mecanografía gratis',
            en: 'touch typing, quick typing, test typing speed, touch typing test'
        }
    },

    competitions: {
        val: {
            templateUrl: 'partials/competitions.html',
            controller: 'CompetitionsController',
            resolve: initializeUser
        },
        params: '',
        langs: {
            en: 'touch-typing-competitions',
            es: 'competitiones-en-mecanografia',
            pl: 'zawody-w-pisaniu-na-klawiaturze'
        },
        title: {
            pl: 'Zawody w szybkim pisaniu na klawiaturze',
            es: 'Competitiones en mecanografía',
            en: 'Touch typing competitions'
        },
        keywords: {
            pl: 'pisanie bezwzrokowe, szybkie pisanie, zawody w szybkim pisaniu, mistrz klawiatury',
            es: 'mecanografía, teclar rápido, competitiones de mecanografía, competitiones de velocidad al teclado',
            en: 'touch typing, quick typing, touch typing competitions, typing master'
        }
    },

    competition: {
        val: {
            templateUrl: 'partials/competition-write.html',
            controller: 'CompetitionController',
            resolve: initializeUser
        },
        params: '/:competitionId/:key?',
        langs: {
            en: 'touch-typing-competition',
            es: 'competition-en-mecanografia',
            pl: 'szybkie-pisanie-zawody'
        },
        title: {
            pl: 'Zawody w szybkim pisaniu na klawiaturze',
            es: 'Competitiones de velocidad al teclado',
            en: 'Touch typing competition'
        },
        keywords: {
            pl: 'pisanie bezwzrokowe, szybkie pisanie, zawody w szybkim pisaniu, mistrz klawiatury',
            es: 'mecanografía, teclar rápido, competitiones de mecanografía, competitiones de velocidad al teclado',
            en: 'touch typing, quick typing, touch typing competitions, typing master'
        }
    },

    login: {
        val: {
            templateUrl: 'partials/login.html',
            controller: 'LoginController',
            resolve: initializeUser
        },
        params: '',
        langs: {
            en: 'sign-in-for-touch-typing',
            es: 'inicia-session-para-mecanografia',
            pl: 'zaloguj-sie-do-pisania-bezwzrokowego'
        },
        title: mainTitle,
        keywords: mainKeywords
    },

    dashboard: {
        val: {
            templateUrl: 'partials/dashboard.html',
            controller: 'DashboardController',
            resolve: initializeUser
        },
        params: '',
        langs: {
            en: 'your-dashboard',
            es: 'tu-perfil',
            pl: 'twoj-profil'
        },
        title: mainTitle,
        keywords: mainKeywords
    },

    profile: {
        val: {
            templateUrl: 'partials/dashboard-other.html',
            controller: 'DashboardOtherController',
            resolve: initializeUser
        },
        params: '/:id',
        langs: {
            en: 'touch-typing-results',
            es: 'resultados-de-mecanografia',
            pl: 'wyniki-szybkiego-pisania'
        },
        title: mainTitle,
        keywords: mainKeywords
    },

    falling_letters: {
        val: {
            templateUrl: 'partials/games/falling-letters.html',
            controller: 'FallingLettersController',
            resolve: initializeUser
        },
        params: '',
        langs: {
            en: 'games-to-learn-touch-typing-falling-letters',
            es: 'juegos-para-aprender-mecanografia-letras-que-caen',
            pl: 'gry-do-nauki-pisania-bezwzorkowego-spadajace-litery'
        },
        title: mainTitle,
        keywords: mainKeywords
    },

    benefits: {
        val: {
            templateUrl: 'partials/articles/article.html',
            controller: 'ArticleController',
            resolve: initializeUser
        },
        params: '',
        langs: {
            en: 'touch-typing-benefits',
            es: 'mecanografia-beneficios',
            pl: 'pisanie-bezwzrokowe-korzysci'
        },
        title: {
            pl: 'Jak uczyć się pisania bezwzrokowego',
            es: 'Como aprender mecanografía',
            en: 'How to learn touch typing'
        },
        keywords: {
            pl: 'pisanie bezwzrokowe korzyści, szybkie pisanie korzyści, korzyści z szybkiego pisania, mistrz klawiatury, pisanie bezwzrokowe',
            es: 'mecanografía beneficios, ventajas de mecanografía, teclar rápido, beneficios de mecanografía',
            en: 'touch typing benefits, why is touch typing beneficial, benefits of touch typing, touch typing advantages, quick typing'
        }
    },

    howtolearn: {
        val: {
            templateUrl: 'partials/articles/article.html',
            controller: 'ArticleController',
            resolve: initializeUser
        },
        params: '',
        langs: {
            en: 'how-to-learn-touch-typing',
            es: 'como-aprender-mecanografia',
            pl: 'jak-uczyc-sie-szybkiego-pisania-bezwzrokowego'
        },
        title: {
            pl: 'Jak uczyć się pisania bezwzrokowego',
            es: 'Como aprender mecanografía',
            en: 'How to learn touch typing'
        },
        keywords: {
            pl: 'nauka szybkiego pisania, kurs szybkiego pisania, kurs pisania bezwzrokowego, jak pisać bez patrzenia, mistrz klawiatury',
            es: 'aprender a tipear, aprender a teclar rápido, curso de mecanografía, como teclar sin mirar el teclado',
            en: 'learn touch typing, touch typing course, quick typing course, how to type without looking, keyboard master'
        }
    },

    school: {
        val: {
            templateUrl: 'partials/school/school.html',
            controller: 'SchoolController',
            resolve: initializeUser
        },
        params: '',
        langs: {
            en: 'touch-typing-school',
            es: 'escuela-de-mecanografia',
            pl: 'szkolenie-z-pisania-bezwzrokowego'
        },
        title: {
            pl: 'Szkolenie z pisania bezwzrokowego',
            es: 'Curso de mecanografia - escuela',
            en: 'Keyboard typing school'
        },
        keywords: {
            pl: 'szkolenie z pisania bezwzrokowego, kurs pisania na klawiaturzre dla firm, szkolenie z szybkiego pisania na klawiaturze',
            es: 'escuela de mecanografia, curso de tipeo para negocios, aprende mecanografiar en grupo',
            en: 'touch typing school, keyboard typing course for companies, type fast in group, typing training for schools'
        }
    },

    group: {
        val: {
            templateUrl: 'partials/school/group.html',
            controller: 'GroupController',
            resolve: initializeUser
        },
        params: '/:groupId?',
        langs: {
            en: 'your-group',
            es: 'tu-grupo',
            pl: 'twoja-grupa'
        },
        title: {
            pl: 'Szkoła pisania bezwzrokowego',
            es: 'Escuela de mecanografia',
            en: 'Typing school'
        },
        keywords: mainKeywords
    }

};
