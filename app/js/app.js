'use strict';

// should be initialized before angular app started to know login state
var initializeUser = {
    user: ['$q', 'userService', 'langService', 'groupService', function ($q, userService, langService, groupService) {
        //call user first to set it first, it will have the logged in user language or the one retrieved from navigator
        var user = userService.getCurrentUser();
        if (user.id === undefined) {
            //user id should be 0 if not logged in
            userService.logout(function () {
                window.location.reload(true);
            });
        }
        var domainLang = langService.getDomainLang();
        if (domainLang) {
            langService.changeSiteLang(domainLang);
        } else {
            langService.changeSiteLang(user.site_lang);
        }

        if (user.id > 0) {
            //only when logged in, otherwise we can't determine group
            var lastGroup = groupService.getFirstToDisplay();
            if (lastGroup !== null) {
                user.last_group = lastGroup;
                userService.showGroupInMenu();
            } else if (user.last_group === undefined) {
                //so the user didn't log in after groups update and there was nothing in local storage - get from db
                var deferred = $q.defer();
                groupService.getLastGroupIdForUser(user.id, function (groupId) {
                    if (groupId) {
                        groupService.setFirstToDisplay(groupId);
                        user.last_group = groupId;
                        userService.showGroupInMenu();
                    }
                    deferred.resolve(user);
                });
                return deferred.promise;
            }
        }

        return user;
    }]
}

var localStorageReady = false;

angular.module('myApp.controllers', []);
angular.module('myApp.services', []);
angular.module('myApp', //
    ['ngRoute', 'ui.bootstrap', 'imageupload', 'angular-momentjs',
        'googlechart', 'xeditable', 'myApp.filters', 'myApp.services', 'myApp.directives',
        'myApp.controllers', 'pascalprecht.translate', 'angular-intro', 'angular-inview', 'xdLocalStorage'])
// routing
    .config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
        for (var routeId in ROUTES) {
            var route = ROUTES[routeId];
            $routeProvider.when('/' + routeId + route.params, route.val);
            for (var lang in route.langs) {
                var val = route.val;
                val.key = routeId;
                $routeProvider.when('/' + route.langs[lang] + route.params, val);
            }
        }
        $routeProvider.otherwise({
            redirectTo: './'
        });

        $locationProvider.html5Mode(true).hashPrefix('!');

    }])
// config local storage
    .config(['xdLocalStorageProvider', function (xdLocalStorageProvider) {
        xdLocalStorageProvider.init(
            {
                iframeUrl: Helpers.getMainDomain() + 'local-storage.html',
                initCallback: function () {
                    //needed for email login when clicking the link
                    localStorageReady=true;
                }
            });
    }]).
// to possibly avoid infinite digest on ie
    config(['$rootScopeProvider', function ($rootScopeProvider) {
        $rootScopeProvider.digestTtl(20);
    }])
// translations
    .config(['$translateProvider', function ($translateProvider) {
        $translateProvider.useStaticFilesLoader({
            prefix: 'data/translations/',
            suffix: '.json'
        });
        $translateProvider.fallbackLanguage('en');
        $translateProvider.preferredLanguage('en');

    }])
// authorization/authentication interceptor
    .config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push('authInterceptor');
        $httpProvider.interceptors.push('loaderInterceptor');
    }])
// inplace editor, executed before user initialization
    .run(['$rootScope', 'errorService', 'editableOptions', 'loader', '$route', 'userService',//
        function ($rootScope, errorService, editableOptions, loader, $route, userService) {
            editableOptions.theme = 'bs3'; // bootstrap3 theme

            $rootScope.$on("$routeChangeStart", function () {
                errorService.cleanError();
                loader.reset();// for ie to not fail on inf digest
                loader.start();
            });

            $rootScope.$on("$routeChangeSuccess", function (event, currentRoute) {
                var route = ROUTES[currentRoute.key];
                if (!route) {
                    route = ROUTES['index'];
                }
                var lang = userService.getSiteLang();
                $rootScope.title = route.title[lang];
                $rootScope.keywords = route.keywords[lang];
                $rootScope.langs = route.langs;

                loader.complete();

                var key = $route.current.key;
                if (!key) {
                    //in case it was called with base url (eg. login), is not always set - on default redirect its not
                    var originalPath = $route.current.originalPath;
                    if (originalPath) {
                        key = originalPath.slice(1);
                    }
                }
                if (Helpers.shouldShowPersonalMessages(key)) {
                    if (!userService.isCurrentUserLoggedIn()) {
                        errorService.messageNotLoggedIn();
                    }
                    errorService.showDisableAdblockMessageIfNeeded();
                }
            });

        }]);
