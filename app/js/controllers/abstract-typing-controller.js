'use strict';

function AbstractTypingController($scope, user, $interval, $timeout) {

    $scope.user = user;
    $scope.clock;
    $scope.finished = false;
    $scope.canPutFocus = false;
    $scope.line = new Line('', 1);

    $scope.focusInput = function () {
        // flag used to prevent writing before overlay is removed
        if ($scope.canPutFocus) {
            $('#writeInput').focus();
        }
    }

    $scope.resetTime = function (newMinutes) {
        $scope.line.stats.timeToWrite = newMinutes * 60;
    }

    $scope.startClockIfNeeded = function () {
        if (!angular.isDefined($scope.clock)) {
            $scope.startClock();
        }
    }

    $scope.stopClock = function () {
        $interval.cancel($scope.clock);
        $scope.clock = undefined;
    }

    $scope.$on('$locationChangeStart', function () {
        $scope.stopClock();
    });

    $scope.updateOnPaste = function () {
        $timeout(function () {
            $scope.line.writtenText = 'Ctrl+C & Ctrl+V !';
            $scope.stopClock();
        }, 50);

    }
}

// no need to define as it's never injected
