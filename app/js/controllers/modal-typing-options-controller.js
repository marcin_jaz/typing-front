function ModalTypingOptionsController($scope, $modal, typingConfigService) {

    $scope.userId;
    $scope.letters;

    $scope.open = function (mode) {
        var config = typingConfigService.getConfig();

        var template = mode;
        if (mode === 'competition') {
            template = 'write';// the same template but displayed differently
        } else if (mode === 'game') {
            template = 'learn';
        }
        var modalInstance = $modal.open({
            templateUrl: './partials/modals/modal-' + template + '-options.html',
            controller: 'ModalTypingOptionsInstanceController',
            resolve: {
                config: function () {
                    // copy to prevent cancel from changing data
                    return angular.copy(config);
                },
                showPutFocus: function () {
                    return mode !== 'competition';
                },
                showTime: function () {
                    return true;// mode !== 'game';
                }
            }

        });

        modalInstance.result.then(function (config) {
            typingConfigService.setConfig(config);
            if (typingConfigService.configHasChanged()) {
                $scope.reloadNewConfig();
                typingConfigService.saveConfig();
            }
        });
    };
};

angular.module('myApp.controllers').controller('ModalTypingOptionsController',
    ['$scope', '$modal', 'typingConfigService', ModalTypingOptionsController]);


function ModalTypingOptionsInstanceController($scope, $modalInstance, textsService, config, showPutFocus, showTime) {

    $scope.config = config;
    $scope.showPutFocus = showPutFocus;
    $scope.showTime = showTime;

    var mark = function () {
        var toMark = config.learn_config;
        if (LAST_LETTER_INDEX_ALWAYS_INCLUDED > toMark) {
            toMark = LAST_LETTER_INDEX_ALWAYS_INCLUDED;
        }
        for (var int = 0; int <= toMark; int++) {
            $scope.letters[int].include = true;
        }
        for (var int = toMark + 1; int < $scope.letters.length; int++) {
            $scope.letters[int].include = false;
        }
    };

    var initialize = function () {
        $scope.letters = new Array();
        var langLetters = textsService.getLetters();
        for (var index in langLetters) {
            var group = langLetters[index];
            $scope.letters.push(new LettersGroup(group));
            if (index < LAST_LETTER_INDEX_ALWAYS_INCLUDED) {
                $scope.letters[index].change = false;
            }
        }
    };
    initialize();
    mark();

    $scope.changeGroup = function (index) {
        if (index > 0) {
            config.learn_config = index;
        } else {
            config.learn_config = 1;
        }
        mark();
    };

    $scope.ok = function () {
        $modalInstance.close($scope.config);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
        $('#writeInput').focus();
    };
};

angular
    .module('myApp.controllers').controller('ModalTypingOptionsInstanceController',
    ['$scope', '$modalInstance', 'textsService', 'config', 'showPutFocus', 'showTime', ModalTypingOptionsInstanceController]);
