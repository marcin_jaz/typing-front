function LearnController($scope, $timeout, textsService, typingConfigService, user, approachService,
                         tooltipTourService, $translate, $modal, $interval, errorService) {

    AbstractTypingController($scope, user, $interval, $timeout);

    errorService.messageSuccess('LEARN_INFO');
    $scope.config = {};
    $scope.letterChangeClass = '';// letter-added | letter-removed | remove

    var initForConfig = function () {
        var text = textsService.getForLearn($scope.config);
        var resultFactor = approachService.getResultFactorForLearn($scope.config.learn_config);
        $scope.line = new Line(text, resultFactor);
        $scope.resetTime($scope.config.learn_minutes);
        $scope.canPutFocus = true;
        $scope.focusInput();
    }

    var initialize = function () {
        typingConfigService.loadConfig($scope.user, function (config) {
            $scope.config = config;
            $scope.config.letters_number = textsService.getLettersNumber().toString();
            initForConfig();
        });
    }

    $scope.reload = function () {
        errorService.showDisableAdblockMessageIfNeeded();
        $scope.stopClock();
        $scope.finished = false;
        initForConfig();
    }

    $scope.reloadNewConfig = function () {
        $scope.stopClock();
        $scope.finished = false;
        initialize();
    }

    var saveApproach = function () {
        approachService.saveLearnApproach($scope.user.id, $scope.config, $scope.line.stats);
        if (typingConfigService.configHasChanged()) {
            typingConfigService.saveConfig();
        }
    }

    var updateWithTime = function () {
        $scope.line.updateStatsWithTime();
        if ($scope.line.hasFinished()) {
            $scope.stopClock();
            $scope.finished = true;
            saveApproach();
        }
    }

    $scope.startClock = function () {
        $scope.clock = $interval(updateWithTime, 1000);
    }

    var showInfoAndRestart = function () {
        // approachService.storeTestTaken();
        $scope.stopClock();
        $translate(['LEARN_BAD_ACCURACY_INFO', 'WAIT_A_MOMENT']).then(function (t) {
            var modalInstance = $modal.open({
                templateUrl: './partials/modals/modal-info.html',
                controller: 'ModalInfoInstanceController',
                resolve: {
                    data: function () {
                        return {
                            title: t.WAIT_A_MOMENT,
                            message: t.LEARN_BAD_ACCURACY_INFO
                        };
                    }
                }

            });

            modalInstance.result.then(function () {
                $scope.reload();
            });
        });
    }

    var setMarkOnLettersChange = function (what) {
        $scope.letterChangeClass = what;
        $timeout(function () {
            $scope.letterChangeClass = 'remove';
        }, 1500);
    }

    var doIntelligentConfigUpdate = function () {
        if ($scope.line.isBadAccuracy()) {
            showInfoAndRestart();
            return;
        }

        $scope.config.focus_letter = $scope.line.getLetterToPutFocusOn();
        $scope.line.fuzzyErrors();
        if ($scope.line.isGoodPerformance()) {
            $scope.config.learn_config = typingConfigService.makeLearnHarder();
            var newFactor = approachService.getResultFactorForLearn($scope.config.learn_config);
            var changed = $scope.line.ajustResultFactor(newFactor);
            if (changed) {
                setMarkOnLettersChange('letter-added');
            }
        } else if ($scope.line.isBadPerformance()) {
            $scope.config.learn_config = typingConfigService.makeLearnEasier();
            var newFactor = approachService.getResultFactorForLearn($scope.config.learn_config);
            var changed = $scope.line.ajustResultFactor(newFactor);
            if (changed) {
                setMarkOnLettersChange('letter-removed');
            }
        }
    };

    $scope.updateOnChange = function () {
        $scope.startClockIfNeeded();
        $scope.line.updateStatsOnChange();
        if ($scope.line.shouldAppendText()) {
            doIntelligentConfigUpdate();
            var text = textsService.getForLearn($scope.config);
            $scope.line.appendText(text);
        }
    }

    initialize();

    $scope.doStartHelpTour = function () {
        tooltipTourService.getTooltipTourForLearn(function (r) {
            $scope.introOptions = r;
            $timeout(function () {
                $scope.startHelpTour();
            }, 50);
        });
    }

}

angular
    .module('myApp.controllers')
    .controller(
    'LearnController',
    ['$scope', '$timeout', 'textsService', 'typingConfigService', 'user', 'approachService', 'tooltipTourService', '$translate', '$modal', '$interval', 'errorService', LearnController]);
