function FallingLettersController($scope, $interval, textsService, typingConfigService, user, $translate, $document) {

    var TOP_MARGIN_TO_HIDE = 455;// as height is 500

    $scope.user = user;
    $scope.config;
    $scope.finished = false;
    $scope.started = false;
    $scope.game;

    $scope.clock;

    var nextId = 0;
    var speed = 100;// the higher the value is the quicker letters move

    var removeTyped = function (pressed) {
        for (i in $scope.letters) {
            var letter = $scope.letters[i];
            if (letter.canBeRemoved(pressed)) {
                $scope.letters.splice(i, 1);
                return true;
            }
        }
        return false;
    }

    $document.bind('keypress', function (e) {
        if (!$scope.started) {
            if (e.which === KEY_ENTER) {
                $scope.$apply(function () {
                    $scope.clock = $interval(updateWithTime, 500);
                    $scope.started = true;
                });
                return;
            }
        } else {
            var pressed = String.fromCharCode(e.which);
            $scope.$apply(function () {
                var wasRemoved = removeTyped(pressed);
                $scope.game.letterTyped(pressed);
                if (!wasRemoved) {
                    $scope.game.addExcessiveError(pressed);
                }
                $scope.game.updateLevel();
                $scope.config.focus_letter = $scope.game.getLetterToPutFocusOn();
            });
        }
    });

    var initForConfig = function () {
        $scope.letters = new Array();
        addNewLetter();
        // TODO think about result factor, 1 for now
        $scope.game = new Game(2);
        $scope.game.setTimeToWrite($scope.config.learn_minutes);
        // workaround to make directive work
        // TODO parametrize directive - doesnt work like with keyboard!
        $scope.line = $scope.game.line;
    }

    var clearNotTyped = function () {
        for (i in $scope.letters) {
            var v = $('#' + $scope.letters[i].id).css('margin-top');
            var top = parseInt(v.slice(0, -2));
            // console.log($scope.letters[i].id + ': ' + top);
            if (top > TOP_MARGIN_TO_HIDE) {
                $scope.game.addMissedError($scope.letters[i].value);
                $scope.letters.splice(i, 1);
            }
        }
    }

    var addNewLetter = function () {
        var width = $('#falling-letters').width() - 80;
        var letter = textsService.getForFallingLetters($scope.config);
        var x = Helpers.getRandomInt(0, width);
        $scope.letters.push(new FallingLetter(letter, x, nextId++));
    }

    var getNumberOfLettersToShow = function () {
        return Math.floor($scope.game.level / 2) + 3;
    }

    var updateWithTime = function () {
        clearNotTyped();
        if ($scope.letters.length < getNumberOfLettersToShow()) {
            addNewLetter();
            setFinishAtTheEnd(-1);
        } else {
            setFinishAtTheEnd(0);
        }
        $scope.game.updateWithTime();
        speed = $scope.game.getSpeed();

        if ($scope.game.hasFinished()) {
            $scope.stopClock();
            $scope.finished = true;
            $scope.letters = new Array();//remove all letters
            // saveApproach();
            // TODO MJ save approach
        }
    }

    var setFinishAtTheEnd = function (which) {
        var lastIndex = $scope.letters.length - 1 + which;
        if (lastIndex >= 0) {
            var y = Helpers.getRandomInt(490, speed);
            $scope.letters[lastIndex].updateYifPossible(y);
        }
    }

    var initialize = function () {
        typingConfigService.loadConfig($scope.user, function (config) {
            $scope.config = config;
            $scope.config.letters_number = textsService.getLettersNumber().toString();
            initForConfig();
        });
    }

    $scope.stopClock = function () {
        $interval.cancel($scope.clock);
        $scope.clock = undefined;
        $scope.started = false;
    }
    $scope.reload = function () {
        $scope.stopClock();
        initForConfig();
    }

    $scope.reloadNewConfig = function () {
        $scope.stopClock();
        initialize();
    }

    initialize();
}

angular
    .module('myApp.controllers')
    .controller(
    'FallingLettersController',
    ['$scope', '$interval', 'textsService', 'typingConfigService', 'user', '$translate', '$document', FallingLettersController]);
