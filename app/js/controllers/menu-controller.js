function MenuController($scope, $route, userService, groupService) {

    var updateLoggedInState = function () {
        $scope.isLoggedIn = userService.isCurrentUserLoggedIn();
        $scope.user = userService.getCurrentUser();
    }
    updateLoggedInState();
    userService.registerLoginChangeCallback(updateLoggedInState);

    $scope.mainUrl = userService.getRoute('index');
    $scope.learnUrl = userService.getRoute('learn');
    $scope.writeUrl = userService.getRoute('write');
    $scope.competitionsUrl = userService.getRoute('competitions');
    $scope.loginUrl = userService.getRoute('login');
    $scope.dashboardUrl = userService.getRoute('dashboard');
    $scope.groupUrl = userService.getRoute('group');
    $scope.schoolUrl = userService.getRoute('school');
    $scope.displayGroup = false;
    //needs to be done by callback because the group retrieved after menu is resolved�
    userService.registerGroupAddedCallback(function () {
        $scope.displayGroup = true;
    })

    $scope.logout = function () {
        userService.logout(function () {
            //Helpers.redirectToMainDomain('index');
            groupService.removeFirstToDisplay();
            $route.reload();
        });
    }

    $scope.$on('$routeChangeSuccess', function () {
        $scope.current = $route.current.key;
    });

}

angular.module('myApp.controllers').controller('MenuController', ['$scope', '$route', 'userService', 'groupService', MenuController]);
