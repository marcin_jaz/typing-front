//function ModalConfirmController($scope, $modal) {
//
//    $scope.open = function (okCallback, message) {
//        var modalInstance = $modal.open({
//            templateUrl: './partials/modals/modal-confirm.html',
//            controller: 'ModalConfirmInstanceController',
//            resolve: {
//                message: function () {
//                    return message;
//                }
//            }
//        });
//
//        modalInstance.result.then(function () {
//            okCallback();
//        });
//    }
//};
//
//angular.module('myApp.controllers').controller('ModalConfirmController',
//    ['$scope', '$modal', ModalConfirmController]);


function ModalConfirmInstanceController($scope, $modalInstance, message) {

    $scope.message = message;
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
    $scope.ok = function () {
        $modalInstance.close();
    };

};
angular.module('myApp.controllers').controller('ModalConfirmInstanceController', ['$scope', '$modalInstance', 'message', ModalConfirmInstanceController]);
