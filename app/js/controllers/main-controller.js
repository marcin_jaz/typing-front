function MainController($scope, userService, user) {

    $scope.learnUrl = userService.getRoute('learn');
    $scope.writeUrl = userService.getRoute('write');
    $scope.competitionsUrl = userService.getRoute('competitions');

    $scope.whyUsPath = 'partials/articles/whyus-' + user.site_lang + '.html';
}

angular.module('myApp.controllers')//
    .controller('MainController', ['$scope', 'userService', 'user', MainController]);
