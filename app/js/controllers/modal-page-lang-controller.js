function ModalPageLangController($scope, $modal, userService, $route) {

    $scope.items = TRANSLATION_LANGUAGES;
    $scope.currentSiteLang = userService.getSiteLang();

    $scope.open = function () {
        var modalInstance = $modal.open({
            templateUrl: './partials/modals/modal-page-lang.html',
            controller: 'ModalPageLangInstanceController',
            resolve: {
                items: function () {
                    return $scope.items;
                },
                selectedItem: function () {
                    return $scope.currentSiteLang;
                }
            }
        });


        modalInstance.result.then(function (selectedItem) {
            if ($scope.currentSiteLang != selectedItem) {
                $scope.currentSiteLang = selectedItem;
                userService.changeSiteLang(selectedItem);
                var newRoute = Helpers.getRoute($route.current.key, selectedItem);
                if (userService.isCurrentUserLoggedIn()) {
                    //the sub domain can't change as the user
                    Helpers.redirectToMainDomain(newRoute);
                } else {
                    Helpers.redirectToSubdomain(newRoute, selectedItem);
                }
            }

        });
    }
};

angular.module('myApp.controllers').controller('ModalPageLangController',
    ['$scope', '$modal', 'userService', '$route', ModalPageLangController]);


function ModalPageLangInstanceController($scope, $modalInstance, items, selectedItem) {

    $scope.items = items;
    $scope.selectedItem = {
        id: selectedItem
    };
    // can't be without object, to avoid created scope:
    // http://stackoverflow.com/questions/16265993/setting-and-getting-bootstrap-radio-button-inside-angular-repeat-loop

    $scope.ok = function () {
        $modalInstance.close($scope.selectedItem.id);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
};

angular.module('myApp.controllers').controller('ModalPageLangInstanceController',
    ['$scope', '$modalInstance', 'items', 'selectedItem', ModalPageLangInstanceController]);
