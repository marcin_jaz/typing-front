function GroupController($scope, user, groupService, $routeParams, errorService, userService, $modal, $translate, $location) {

    $scope.orderProperty = 'nick';
    $scope.isReverse = false;

    $scope.othersFilter = {
        name: '',
        archived: true
    }

    $scope.searchUser = {nick: ''};
    $scope.profileUrl = userService.getRoute('profile');
    $scope.viewerIsAdmin = false;
    $scope.userId = user.id;
    $scope.tab = groupService.getLastTab();
    $scope.active = {};
    $scope.active[$scope.tab] = true;
    $scope.viewerIsStudent = false;
    $scope.groups;
    $scope.baseUrl = userService.getRoute('group');

    var hash = $location.hash();
    var redirectedAfterCreation = function () {
        return hash === 'created';
    }

    $scope.otherGroupsFilter = function (g) {
        // if include enabled or if is not archived
        return ($scope.othersFilter.name.length === 0 || g.name.toLowerCase().indexOf($scope.othersFilter.name.toLowerCase()) >= 0)
            && ($scope.othersFilter.archived === true || g.is_archived === 0);
    };

    function getAddUserUrl(group) {
        return Helpers.getMainDomain() + $scope.baseUrl + '/' + group.id + '#' + group.private_key;
    }

    function initializeNameChangeWatcher() {
        $scope.$watch('group.name', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                groupService.changeName($scope.group);
            }
        });
    }

    $scope.fillTab = function () {
        //console.log($scope.tab);
        if (!$scope.active['competitions']) {
            groupService.getStudentsWithStats($scope.group, $scope.tab, function (students) {
                $scope.group.students = students;
            })
        } else if ($scope.active['competitions']) {
            groupService.getPositionsOfCompetitionsInGroup($scope.group.id, function (result) {
                $scope.competitions = result.competitions;
                $scope.positions = result.positions;
            });
        }
    }

    $scope.tabChanged = function (tab) {
        if ($scope.tab !== tab) {
            //must be checked this way - collection $scope.active is unpredictable and driven by tabs directive
            $scope.active = {}; //reset
            $scope.active[tab] = true;
            $scope.tab = tab;
            groupService.setLastTab(tab);
            $scope.fillTab();
        }
    }

    var initializeGroup = function (group) {
        groupService.setFirstToDisplay(group.id);
        group.link = getAddUserUrl(group);
        $scope.viewerIsAdmin = group.created_by === user.id;
        if (group.is_archived) {
            errorService.messageWarning('GROUP_IS_ARCHIVED');
        } else if (redirectedAfterCreation()) {
            errorService.messageSuccess('GROUP_CREATED_MSG', {JOIN_URL: group.link});
            userService.showGroupInMenu();
        }

        groupService.getGroupsForUser(user.id, function (groups) {
            $scope.viewerIsStudent = Helpers.toIds(groups.groups_student).indexOf(group.id) >= 0;
            $scope.groups = groupService.removeDuplicatesFromOtherGroups(groups, group.id);
            $scope.othersFilter.archived = $scope.groups.count < 8;//for little entries show archived by default
        })

        $scope.group = group;
        $scope.fillTab();
        if ($scope.viewerIsAdmin === true) {
            initializeNameChangeWatcher();
        }

    }

    function getGroupById(groupId) {
        groupService.getGroupById(groupId, function (group) {
            if (group) {
                group.is_archived = group.archived_time != null;
                if (!group.is_archived && hash && !redirectedAfterCreation()) {
                    if (userService.isCurrentUserLoggedIn()) {
                        groupService.addUserToGroup($routeParams.groupId, user.id, hash, function (added) {
                            if (added > 0) {
                                errorService.messageSuccess('GROUP_USER_ADDED');
                                userService.showGroupInMenu();
                            } else {
                                errorService.messageWarning('GROUP_ALREADY_ADDED_USER');
                            }
                            initializeGroup(group);
                        }, function () {
                            initializeGroup(group);
                        });
                    } else {
                        errorService.messageError('GROUP_LOG_IN_TO_JOIN');
                        initializeGroup(group);
                    }
                } else {
                    initializeGroup(group);
                }
            } else {
                Helpers.redirectToMainDomain(userService.getRoute('school'));
            }
        });
    }

    if ($routeParams.groupId === undefined) {
        if (user.last_group !== undefined && user.last_group) {
            getGroupById(user.last_group);
        } else {
            Helpers.redirectToMainDomain(userService.getRoute('school'));
        }
    } else {
        getGroupById($routeParams.groupId);
    }

    $scope.orderBy = function (what) {
        if ($scope.orderProperty === what) {
            $scope.isReverse = !$scope.isReverse;
        } else {
            $scope.orderProperty = what;
            //nick by default ascending, others descending - from best to worse
            $scope.isReverse = $scope.orderProperty !== 'nick';
        }
    }
    $scope.orderedBy = function (what) {
        if ($scope.orderProperty === what) {
            if ($scope.isReverse) {
                return ARROW_UP;
            }
            return ARROW_DOWN;
        }
        return '';
    }

    $scope.removeUserFromGroup = function (user) {
        $translate(['GROUP_REMOVE_USER', 'GROUP_REMOVE_USER_INFO']).then(function (t) {
            var modalInstance = $modal.open({
                templateUrl: './partials/modals/modal-confirm.html',
                controller: 'ModalConfirmInstanceController',
                resolve: {
                    message: function () {
                        return {
                            title: t.GROUP_REMOVE_USER + user.nick,
                            message: t.GROUP_REMOVE_USER_INFO
                        };
                    }
                }
            });
            modalInstance.result.then(function () {
                groupService.removeUserFromGroup($scope.group.id, user.id, $scope.group.created_by, function () {
                    $scope.group.students.splice($scope.group.students.indexOf(user), 1);
                })
            });
        });
    }

}

angular.module('myApp.controllers').controller('GroupController', ['$scope', 'user', 'groupService', '$routeParams',
    'errorService', 'userService', '$modal', '$translate', '$location', GroupController]);
