function WriteController($scope, textsService, typingConfigService, user, approachService, tooltipTourService,
                         $interval, errorService, $timeout) {

    AbstractTypingController($scope, user, $interval, $timeout);

    errorService.messageSuccess('WRITE_INFO');
    $scope.config = {};

    var initForConfig = function () {
        var text = textsService.getForWrite($scope.config);
        var resultFactor = approachService.getResultFactorForWrite($scope.config.write_config);
        $scope.line = new Line(text, resultFactor);
        $scope.resetTime($scope.config.write_minutes);
        $scope.canPutFocus = true;
        $scope.focusInput();
    }

    var initialize = function () {
        typingConfigService.loadConfig($scope.user, function (config) {
            $scope.config = config;
            initForConfig();
        });
    }

    $scope.reload = function () {
        errorService.showDisableAdblockMessageIfNeeded();
        $scope.stopClock();
        $scope.finished = false;
        initForConfig();
    }

    $scope.reloadNewConfig = function () {
        $scope.stopClock();
        $scope.finished = false;
        initialize();
    }

    var saveApproach = function () {
        approachService.saveWriteApproach($scope.user.id, $scope.config, $scope.line.stats);
    }

    var updateWithTime = function () {
        $scope.line.updateStatsWithTime();
        if ($scope.line.hasFinished()) {
            $scope.stopClock();
            $scope.finished = true;
            saveApproach();
        }
    }

    $scope.startClock = function () {
        $scope.clock = $interval(updateWithTime, 1000);
    }

    $scope.updateOnChange = function () {
        $scope.startClockIfNeeded();
        $scope.line.updateStatsOnChange();
        if ($scope.line.shouldAppendText()) {
            $scope.config.focus_letter = $scope.line.getLetterToPutFocusOn();
            var text = textsService.getForWrite($scope.config);
            $scope.line.appendText(text);
        }
    }

    initialize();

    $scope.doStartHelpTour = function () {
        tooltipTourService.getTooltipTourForWrite(function (r) {
            $scope.introOptions = r;
            $timeout(function () {
                $scope.startHelpTour();
            }, 50);
        });
    }

}

angular
    .module('myApp.controllers')
    .controller(
        'WriteController',
        ['$scope', 'textsService', 'typingConfigService', 'user', 'approachService', 'tooltipTourService', '$interval', 'errorService', '$timeout', WriteController]);
