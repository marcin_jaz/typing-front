function DashboardController($scope, approachService, user, userService, $location, dashboardService,
                             $translate, competitionService) {

    AbstractDashboardController($scope, approachService, $location, dashboardService, $translate,
        competitionService, userService)

    // redirect to login if not logged in
    if (user.id === undefined || user.id <= 0) {
        $location.path(userService.getRoute('login'));
    }
    $scope.isOwnProfile = true;
    $scope.user = user;
    $scope.loadForUser();

    $scope.$watch('user.nick', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            userService.changeNick(newVal);
        }
    });

    $scope.$watch('newPicture', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            userService.changePicture(newVal);
        }
    });

}

angular.module('myApp.controllers').controller('DashboardController',
    ['$scope', 'approachService', 'user', 'userService', '$location', 'dashboardService', '$translate', 'competitionService', '$routeParams', DashboardController]);
