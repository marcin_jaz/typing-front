function ModalGroupOptionsController($scope, $modal, groupService, $route) {

    $scope.open = function (group) {
        var modalInstance = $modal.open({
            templateUrl: './partials/modals/modal-group-options.html',
            controller: 'ModalGroupOptionsInstanceController',
            resolve: {
                group: function () {
                    return group;
                }
            }
        });

        modalInstance.result.then(function (changedGroup) {
            groupService.updateGroup(changedGroup, function () {
                $route.reload();
            });
        });
    }
};

angular.module('myApp.controllers').controller('ModalGroupOptionsController',
    ['$scope', '$modal', 'groupService', '$route', ModalGroupOptionsController]);


function ModalGroupOptionsInstanceController($scope, $modalInstance, group) {
    $scope.group = group;
    $scope.groupBeforeChanges = angular.copy(group);

    $scope.onLinkChanged = function () {
        group.link = $scope.groupBeforeChanges.link;
    }

    function hasChanged() {
        return $scope.group.name !== $scope.groupBeforeChanges.name
            || $scope.group.show_others !== $scope.groupBeforeChanges.show_others
            || $scope.group.is_archived !== $scope.groupBeforeChanges.is_archived;
    }

    $scope.onLinkClick = function ($event) {
        $event.target.select();
    };

    $scope.ok = function () {
        if (hasChanged()) {
            $modalInstance.close($scope.group);
        } else {
            $scope.cancel();
        }
    };

    $scope.cancel = function () {
        $scope.group.name = $scope.groupBeforeChanges.name;
        $scope.group.show_others = $scope.groupBeforeChanges.show_others;
        $scope.group.is_archived = $scope.groupBeforeChanges.is_archived;
        $modalInstance.dismiss('cancel');
    };
};

angular.module('myApp.controllers').controller('ModalGroupOptionsInstanceController',
    ['$scope', '$modalInstance', 'group', ModalGroupOptionsInstanceController]);
