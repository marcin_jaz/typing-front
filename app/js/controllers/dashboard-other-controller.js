function DashboardOtherController($scope, approachService, user, userService, $location, dashboardService,
                                  $translate, competitionService, $routeParams) {

    AbstractDashboardController($scope, approachService, $location, dashboardService, $translate,
        competitionService, userService)

    $scope.isOwnProfile = false;

    var initializeUser = function () {
        if ($routeParams.id == undefined || $routeParams.id == user.id) {
            $location.path(userService.getRoute('dashboard'));
        } else {
            userService.getById($routeParams.id, function (u) {
                if (u) {
                    $scope.user = u;
                    $scope.loadForUser();
                } else {
                    $location.path(userService.getRoute('index'));
                }
            });
        }
    }
    initializeUser();

}

angular.module('myApp.controllers').controller('DashboardOtherController',
    ['$scope', 'approachService', 'user', 'userService', '$location', 'dashboardService',
        '$translate', 'competitionService', '$routeParams', DashboardOtherController]);
