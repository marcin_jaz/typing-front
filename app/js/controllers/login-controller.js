function LoginController($scope, userService, errorService, $location, $routeParams) {

    var PERSONA = "persona";
    var provider = '';

    if (userService.isCurrentUserLoggedIn()) {
        //if the user is logged in redirect to index
        $location.path(userService.getRoute('dashboard'));
    }

    $scope.loginWith = function (p) {
        provider = p;
        hello(provider).login();
    }

    function loginInternal() {
        if (userService.isCurrentUserLoggedIn()) {
            Helpers.redirectToMainDomain(userService.getRoute('dashboard'));
        }
    }

    hello.init({
        facebook: '745433225525211',
        // live: '745433225525211'
        // localhost: '216409491723718' 745444215524112
        google: '117687605176-d1r549ejm69j7kp1g8qvemuht5lb71gu.apps.googleusercontent.com',
        linkedin: '756eq3a8lgi6kq'
    }, {
        redirect_uri: Helpers.getMainDomainWithCurrentLang(),
        scope: 'email',
        oauth_proxy: 'https://auth-server.herokuapp.com/proxy'
    });
    hello.on('auth.login', function (auth) {
        hello(auth.network).api('/me').then(function (user) {
            if (provider !== "" && provider !== PERSONA) {
                if (user.email) {
                    user.provider = provider;
                    //take the site lang not from langService as it could be changed in the meantime
                    user.site_lang = userService.getSiteLang();
                    userService.login(user, loginInternal);
                } else {// no email
                    // you can log in to fb without email but with phone number
                    // if email is not confirmed it won't be returned neither
                    errorService.messageError('NO_EMAIL');
                }
                provider = "";// to not call that automatically
            }
        });
    });

    $scope.loginWithEmail = function () {
        provider = PERSONA;
        userService.sendEmailForPersona(function (sent) {
            if (sent) {
                errorService.messageSuccess('LOGIN_EMAIL_SENT');
            } else {
                errorService.messageError('LOGIN_EMAIL_NOT_SENT');
            }
        }, $scope.email);
    }

    if ($routeParams.code != null && $routeParams.email != null) {
        userService.loginWithPersona($routeParams.code, $routeParams.email, loginInternal);
    }

}

angular.module('myApp.controllers').controller('LoginController', ['$scope', 'userService', 'errorService', '$location', '$routeParams', LoginController]);
