function AbstractDashboardController($scope, approachService, $location, dashboardService, $translate, competitionService, userService) {

    $scope.modes = [];
    $scope.competitionPositionsHistory = new Array(3);
    $scope.competitionHistory = [];
    $scope.chart = {data: {rows: []}};

    var getZeroSummary = function () {
        return {
            approaches: 0,
            time_typing: 0,
            words_typed: 0,
            avg_accuracy: 0,
            avg_wpm: 0,
            best_wpm: 0
        };
    }
    $scope.summary = getZeroSummary();

    var initializeLabels = function () {
        $translate(['LEARN', 'WRITE', 'COMPETITIONS']).then(function (translations) {
            $scope.modes = [{
                name: translations.LEARN,
                value: '"learn"',
                selected: true
            }, {
                name: translations.WRITE,
                value: '"write"',
                selected: true
            }, {
                name: translations.COMPETITIONS,
                value: '"competition"',
                selected: true
            }];

            $scope.loadSummary();
        });
    }

    var initializeChartLegend = function () {
        $translate(['WPM', 'ACCURACY', 'RESULT']).then(function (translations) {
            $scope.chart = {
                "type": "LineChart",
                "displayed": true,
                "data": {
                    "cols": [{
                        "id": "date",
                        "label": "Date",
                        "type": "string",
                    }, {
                        "id": "wpm",
                        "label": translations.WPM,
                        "type": "number",
                    }, {
                        "id": "accuracy",
                        "label": translations.ACCURACY,
                        "type": "number",
                    }, {
                        "id": "result",
                        "label": translations.RESULT,
                        "type": "number",
                    }, {
                        "id": "id",
                        "role": "tooltip",
                        "type": "string",
                        "p": {
                            "role": "tooltip",
                            "html": true
                        }
                    }],
                },
                "options": {
                    "isStacked": "true",
                    "fill": 20,
                    "displayExactValues": true,
                    "tooltip": {
                        "isHtml": true
                    }
                }
            }
        });
    }

    var retrieveModes = function () {
        var modes = "";
        for (var i in $scope.modes) {
            var mode = $scope.modes[i];
            if (mode.selected == true) {
                modes += mode.value + ",";
            }
        }
        return modes.substr(0, modes.length - 1);
    }

    $scope.loadSummary = function () {
        var modes = retrieveModes();
        approachService.getAllForModes($scope.user.id, modes, function (approaches) {
            $scope.approachesForChart = approaches;
            drawChart();
        });
        approachService.getSummaryForModes($scope.user.id, modes, function (summary) {
            if (summary.approaches > 0) {
                summary.words_typed = (summary.keystrokes / KEYSTROKES_IN_WORD).toFixed();
            } else {
                //needed because some calculations can't be done with zero
                summary = getZeroSummary();
            }
            $scope.summary = summary;
        });
    }

    var loadCompetitionHistory = function () {
        competitionService.getHistory($scope.user.id, function (h) {
            $scope.competitionHistory = h;
        });
        competitionService.getPositionsHistory($scope.user.id, function (ph) {
            $scope.competitionPositionsHistory = ph;
        });
    }

    var drawChart = function () {
        $scope.chart.data.rows = dashboardService.buildChart($scope.approachesForChart);
    }

    $scope.joinCompetition = function (id, private_key) {
        $location.path(userService.getRoute('competition') + "/" + id + "/" + private_key);
    }

    $scope.loadForUser = function () {
        initializeLabels();
        initializeChartLegend();
        loadCompetitionHistory();
    }
}
// no need to define as it's never injected
