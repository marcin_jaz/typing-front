function ArticleController($scope, user, $route) {
    var what = $route.current.key + '-' + user.site_lang;
    $scope.path = 'partials/articles/' + what + '.html';

}

angular.module('myApp.controllers').controller('ArticleController', ['$scope', 'user', '$route', ArticleController]);
