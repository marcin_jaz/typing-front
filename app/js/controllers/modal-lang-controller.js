function ModalLangController($scope, $modal, typingConfigService) {

    $scope.open = function () {
        var config = typingConfigService.getConfig();

        var modalInstance = $modal.open({
            templateUrl: './partials/modals/modal-lang.html',
            controller: 'ModalLangInstanceController',
            resolve: {
                selectedItem: function () {
                    return config.lang;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            if (config.lang != selectedItem) {
                // config here is for the old lang yet
                typingConfigService.setNewLang(selectedItem);
                $scope.reloadNewConfig();
                typingConfigService.changeDefaultLanguage(selectedItem);
            }
        });

    };
};
angular.module('myApp.controllers').controller('ModalLangController', ['$scope', '$modal', 'typingConfigService', ModalLangController]);

function ModalLangInstanceController($scope, $modalInstance, selectedItem) {

    $scope.items = WRITING_LANGUAGES;
    $scope.selectedItem = {
        id: selectedItem
    };
    // can't be without object, to avoid created scope:
    // http://stackoverflow.com/questions/16265993/setting-and-getting-bootstrap-radio-button-inside-angular-repeat-loop

    $scope.ok = function () {
        $modalInstance.close($scope.selectedItem.id);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
        $('#writeInput').focus();
    };
};
angular.module('myApp.controllers').controller('ModalLangInstanceController',
    ['$scope', '$modalInstance', 'selectedItem', ModalLangInstanceController]);
