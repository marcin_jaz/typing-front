function ErrorController($scope, errorService, loader) {

    $scope.messages = new Array();
    $scope.showSpinner;

    $scope.closeAlert = function (index, id) {
        $scope.messages.splice(index, 1);
        errorService.hideMessage(id);
    }

    var setError = function (messages) {
        $scope.messages = messages;
    };
    errorService.registerObserverCallback(setError);

    var changeSpinner = function (shouldShow) {
        $scope.showSpinner = shouldShow;
    }
    loader.registerObserverCallback(changeSpinner);

}

angular.module('myApp.controllers')//
    .controller('ErrorController', ['$scope', 'errorService', 'loader', ErrorController]);
