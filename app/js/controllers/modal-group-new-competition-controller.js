function ModalGroupNewCompetitionController($scope, $modal, competitionService, typingConfigService, textsService) {

    $scope.open = function (group, createdCallback) {

        var config = {
            lang: group.lang,
            write_minutes: 1,
            write_config: 1000,
            private_competition: true
        };

        var modalInstance = $modal.open({
            //TODO MJ only title can be customized and the you can use modal-write-options.html removing below html!!!
            templateUrl: './partials/modals/modal-group-new-competition.html',
            controller: 'ModalTypingOptionsInstanceController',
            resolve: {
                config: function () {
                    // copy to prevent cancel from changing data
                    return angular.copy(config);
                },
                showPutFocus: function () {
                    return false;
                },
                showTime: function () {
                    return true;
                }
            }

        });

        modalInstance.result.then(function (config) {
                var competition = new Competition(config, group.created_by);
                competition.group_id = group.id;

                function createCompetition() {
                    competitionService.saveCompetition(competition, function () {
                        //TODO MJ here competition created msg if needed then
                        createdCallback();
                    });
                }

                var configStored = typingConfigService.getConfig();
                if (configStored == null || (configStored.lang !== group.lang)) {
                    // temporally load other language because the text for competition must be generated
                    // at the same time we don't want to change user language preference
                    textsService.loadLanguageDataFile({lang: group.lang}, function () {
                            createCompetition();
                            if (configStored != null) {
                                //rollback the old file as in the config if it was present
                                textsService.loadLanguageDataFile({lang: configStored.lang});
                            }
                        }
                    )
                } else {
                    createCompetition();
                }
            }
        )
        ;
    };
}
;

angular.module('myApp.controllers').controller('ModalGroupNewCompetitionController',
    ['$scope', '$modal', 'competitionService', 'typingConfigService', 'textsService', ModalGroupNewCompetitionController]);
