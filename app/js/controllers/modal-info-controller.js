function ModalInfoInstanceController($scope, $modalInstance, data) {

    $scope.data = data;
    $scope.ok = function () {
        $modalInstance.close();
    };

};
angular.module('myApp.controllers').controller('ModalInfoInstanceController', ['$scope', '$modalInstance', 'data', ModalInfoInstanceController]);
