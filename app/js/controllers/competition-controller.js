function CompetitionController($scope, user, approachService, competitionService, $routeParams, tooltipTourService,
                               $interval, userService, $timeout, errorService) {

    AbstractTypingController($scope, user, $interval, $timeout);

    $scope.competition = {};
    $scope.showAllPositionsLink = false;
    $scope.positions = [];
    $scope.profileUrl = userService.getRoute('profile');

    var initialize =
        function () {
            competitionService.getCompetition($routeParams.competitionId, $routeParams.key, function (c) {
                $scope.competition = c;
                var resultFactor = approachService.getResultFactorForWrite($scope.competition.typing_words);
                $scope.line = new Line($scope.competition.text, resultFactor);

                $scope.resetTime($scope.competition.typing_minutes);
                $scope.focusInput();
                competitionService.getPositionsOfCompetition($routeParams.competitionId, function (positions) {
                    $scope.initialPositions = positions;
                    // for the beginning it shows the leaders
                    $scope.positions =
                        competitionService.showInitialPositions($scope.initialPositions,
                            COMPETITION_CLASSIFICATION_USERS);
                    $scope.canPutFocus = true;
                    $scope.focusInput();
                });
            });
        }

    $scope.reload = function () {
        errorService.showDisableAdblockMessageIfNeeded();
        $scope.stopClock();
        $scope.finished = false;
        initialize();
    }

    $scope.showAllPositions =
        function () {
            $scope.positions =
                competitionService.calculatePositions($scope.initialPositions, $scope.user,
                    $scope.line.stats.result, Number.MAX_VALUE);
            $scope.showAllPositionsLink = false;
        }

    var updateWithTime =
        function () {
            $scope.line.updateStatsWithTime();
            $scope.positions =
                competitionService.calculatePositions($scope.initialPositions, $scope.user,
                    $scope.line.stats.result, COMPETITION_CLASSIFICATION_USERS);
            if ($scope.line.hasFinished()) {
                finishLine();
            }
        }

    $scope.startClock = function () {
        $scope.clock = $interval(updateWithTime, 1000);
    }

    var finishLine = function () {
        $scope.stopClock();
        $scope.finished = true;
        $scope.showAllPositionsLink = $scope.initialPositions.length + 1 > COMPETITION_CLASSIFICATION_USERS;
        saveApproach();
    }

    var saveApproach = function () {
        approachService.saveCompetitionApproach($scope.user.id, $scope.competition, $scope.line.stats);
    }

    $scope.updateOnChange = function () {
        $scope.startClockIfNeeded();
        $scope.line.updateStatsOnChange();
    }

    initialize();

    $scope.doStartHelpTour = function () {
        tooltipTourService.getTooltipTourForCompetition(function (r) {
            $scope.introOptions = r;
            $timeout(function () {
                $scope.startHelpTour();
            }, 50);
        });
    }

}

angular.module('myApp.controllers')
    .controller(
        'CompetitionController',
        ['$scope', 'user', 'approachService', 'competitionService', '$routeParams', 'tooltipTourService', '$interval',
            'userService', '$timeout', 'errorService', CompetitionController]);
