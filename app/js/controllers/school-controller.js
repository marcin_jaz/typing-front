function SchoolController($scope, user, typingConfigService, groupService, $location, userService, $translate) {
    var what = 'school-text-' + user.site_lang;
    $scope.path = 'partials/school/' + what + '.html';

    $scope.user = user;
    $scope.notLoggedIn = user.id == 0;

    $translate('CREATED').then(function (t) {
        $scope.groupName = t;
    });
    $scope.reloadNewConfig = function () {
        typingConfigService.loadConfig($scope.user, function (config) {
            $scope.config = config;
        });
    }
    $scope.reloadNewConfig();

    $scope.createGroup = function () {
        var group = {
            'userId': $scope.user.id,
            'groupName': $scope.groupName,
            'lang': $scope.config.lang,
            'privateKey': Helpers.createPrivateKey()
        };
        groupService.createGroup(group, function (id) {
            $location.path(userService.getRoute('group') + '/' + id);
            $location.hash('created');
        });
    }
}

angular.module('myApp.controllers').
    controller('SchoolController', ['$scope', 'user', 'typingConfigService', 'groupService', '$location', 'userService', '$translate',
        SchoolController]);