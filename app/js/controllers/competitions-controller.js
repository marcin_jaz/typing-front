function CompetitionsController($scope, competitionService, typingConfigService, user, $location, errorService, userService) {

    $scope.competitions;
    $scope.config;

    competitionService.getAll(function (competitions) {
        $scope.competitions = competitions;
    });

    $scope.reloadNewConfig = function () {
        var isNowPrivate = false;
        if ($scope.config && $scope.config.private_competition !== undefined) {
            isNowPrivate = $scope.config.private_competition;
        }
        typingConfigService.loadConfig(user, function (config) {
            $scope.config = config;
            $scope.config.private_competition = isNowPrivate;
        });
    }
    $scope.reloadNewConfig();

    $scope.createCompetition = function () {
        var competition = new Competition($scope.config, user.id);
        competitionService.saveCompetition(competition, function (id) {
            if (competition.isPrivate()) {
                joinPrivateCompetition(id, competition.private_key);
            } else {
                $scope.joinCompetition(id);
            }
        });
    }

    var joinPrivateCompetition = function (id, key) {
        errorService.messageSuccess('PRIVATE_COMPETITION_CREATED');
        $location.path(userService.getRoute('competition') + '/' + id + "/" + key);
    }

    $scope.joinCompetition = function (id) {
        $location.path(userService.getRoute('competition') + '/' + id);
    }

    $scope.canCreateNewCompetition = function () {
        return user.id > 0;
    }
    // this must be variable to use in html
    $scope.newCompetitionOff = !$scope.canCreateNewCompetition();
}

angular
    .module('myApp.controllers')
    //
    .controller(
    'CompetitionsController',
    ['$scope', 'competitionService', 'typingConfigService', 'user', '$location', 'errorService', 'userService', CompetitionsController]);
