describe('line', function() {

	describe('stats update errors and line finish', function() {

		it('should return one error on wrong last letter', function() {
			// given
			var line = new Line('abcd ');
			line.writtenText = 'abce ';
			// when
			var stats = line.updateStatsOnChange();
			// then
			expect(stats.errors).toEqual(1);
			expect(line.isFinished).toBe(true);
		});

		it('should return one error on wrong letter in the middle', function() {
			// given
			var line = new Line('abcd ');
			line.writtenText = 'axcd ';
			// when
			var stats = line.updateStatsOnChange();
			// then
			expect(stats.errors).toEqual(1);
			expect(line.isFinished).toBe(true);
		});

		it('should return one error on wrong letter at the beginning', function() {
			// given
			var line = new Line('abcd ');
			line.writtenText = 'xbcd ';
			// when
			var stats = line.updateStatsOnChange();
			// then
			expect(stats.errors).toEqual(1);
			expect(line.isFinished).toBe(true);
		});

		it('should return one error on missing letter at the beginning', function() {
			// given
			var line = new Line('abcd ');
			line.writtenText = 'bcd ';
			// when
			var stats = line.updateStatsOnChange();
			// then
			expect(stats.errors).toEqual(1);
			expect(line.isFinished).toBe(true);
		});

		it('should return one error on missing letter in the middle', function() {
			// given
			var line = new Line('abcde ');
			line.writtenText = 'abde ';
			// when
			var stats = line.updateStatsOnChange();
			// then
			expect(stats.errors).toEqual(1);
			expect(line.isFinished).toBe(true);
		});

		it('should return no error on missing letter at the end', function() {
			// given
			var line = new Line('abcde ');
			line.writtenText = 'abcd ';
			// when
			var stats = line.updateStatsOnChange();
			// then
			expect(stats.errors).toEqual(1);
			expect(line.isFinished).toBe(true);
		});

		it('should return one error on missing letter and not finished', function() {
			// given
			var line = new Line('abcde ');
			line.writtenText = 'acd';
			// when
			var stats = line.updateStatsOnChange();
			// then
			expect(stats.errors).toEqual(1);
			expect(line.isFinished).toBe(false);
		});

		it('should return two errors when one missed and one too much', function() {
			// given
			var line = new Line('abcde ');
			line.writtenText = 'acxde ';
			// when
			var stats = line.updateStatsOnChange();
			// then
			expect(stats.errors).toEqual(2);
			expect(line.isFinished).toBe(true);
		});

		it('should return two errors when two missed', function() {
			// given
			var line = new Line('abcde ');
			line.writtenText = 'ace ';
			// when
			var stats = line.updateStatsOnChange();
			// then
			expect(stats.errors).toEqual(2);
			expect(line.isFinished).toBe(true);
		});
		it('should return two errors when one missed and last same as missing', function() {
			// given
			var line = new Line('abcde ');
			line.writtenText = 'acee ';
			// when
			var stats = line.updateStatsOnChange();
			// then
			expect(stats.errors).toEqual(2);
			expect(line.isFinished).toBe(true);
		});

		it('should propagate errors even if good letter written for finish', function() {
			// given
			var line = new Line('abcde ');
			line.writtenText = 'xbdfse';
			// when
			var stats = line.updateStatsOnChange();
			// then
			expect(stats.errors).toEqual(5);
			expect(line.isFinished).toBe(true);
		});

		it('should propagate errors when shift more than one and finished', function() {
			// given
			var line = new Line('abcde ');
			line.writtenText = 'xbdfs';
			// when
			var stats = line.updateStatsOnChange();
			// then
			expect(stats.errors).toEqual(4);
			expect(line.isFinished).toBe(true);
		});

		it('should return three in case of all 3 kinds of errors', function() {
			// given
			var line = new Line('abcde. ');
			line.writtenText = 'xbdfe. ';
			// when
			var stats = line.updateStatsOnChange();
			// then
			expect(stats.errors).toEqual(3);
			expect(line.isFinished).toBe(true);
		});

		it('should deal with two line code', function() {
			// given
			var line = new Line('ab cd ');
			line.writtenText = 'ab cd ';
			// when
			var stats = line.updateStatsOnChange();
			// then
			expect(stats.errors).toEqual(0);
			expect(line.isFinished).toBe(true);
		});
	});

	describe('stats update corrections', function() {

		it('should correct one error at the end', function() {
			// assume
			var line = new Line('abcd ');
			line.writtenText = 'abcx';
			line.updateStatsOnChange();
			// given
			line.writtenText = 'abcd';
			// when
			var stats = line.updateStatsOnChange();
			// then
			expect(stats.errors).toEqual(0);
			expect(stats.corrections).toBe(1);
		});

		it('should correct one error in the middle', function() {
			// assume
			var line = new Line('abcd ');
			line.writtenText = 'abxd';
			line.updateStatsOnChange();
			// given
			line.writtenText = 'abcd';
			// when
			var stats = line.updateStatsOnChange();
			// then
			expect(stats.errors).toEqual(0);
			expect(stats.corrections).toBe(1);
		});

		it('should correct missed letter', function() {
			// assume
			var line = new Line('abcd ');
			line.writtenText = 'abd';
			line.updateStatsOnChange();
			// given
			line.writtenText = 'abcd';
			// when
			var stats = line.updateStatsOnChange();
			// then
			expect(stats.errors).toEqual(0);
			expect(stats.corrections).toBe(1);
		});

		it('should correct excessive letter', function() {
			// assume
			var line = new Line('abcd ');
			line.writtenText = 'abcxd';
			line.updateStatsOnChange();
			// given
			line.writtenText = 'abcd';
			// when
			var stats = line.updateStatsOnChange();
			// then
			expect(stats.errors).toEqual(0);
			expect(stats.corrections).toBe(1);
		});

		it('should correct error at the beginning', function() {
			// assume
			var line = new Line('abcd ');
			line.writtenText = 'bcd';
			line.updateStatsOnChange();
			// given
			line.writtenText = 'abcd';
			// when
			var stats = line.updateStatsOnChange();
			// then
			expect(stats.errors).toEqual(0);
			expect(stats.corrections).toBe(1);
		});

		it('should correct all kinds of errors', function() {
			// assume
			var line = new Line('abcdef ');
			line.writtenText = 'xbdexf';
			line.updateStatsOnChange();
			// given
			line.writtenText = 'abcdef';
			// when
			var stats = line.updateStatsOnChange();
			// then
			expect(stats.errors).toEqual(0);
			expect(stats.corrections).toBe(3);
		});

		it('should correct all kinds of errors when text erased', function() {
			// assume
			var line = new Line('abcdef ');
			line.writtenText = 'xbdexf';
			line.updateStatsOnChange();
			// given
			line.writtenText = '';
			// when
			var stats = line.updateStatsOnChange();
			// then
			expect(stats.errors).toEqual(0);
			expect(stats.corrections).toBe(3);
		});

	});

	describe('keystrokes count', function() {

		it('should return one', function() {
			var line = new Line('a');
			var keystrokes = line.countKeystrokes('a');
			expect(keystrokes).toEqual(1);
		});

		it('should return two on two letters', function() {
			var line = new Line('a');
			var keystrokes = line.countKeystrokes('ab');
			expect(keystrokes).toEqual(2);
		});

		it('should return two on double keystroke letter', function() {
			var line = new Line('a');
			var keystrokes = line.countKeystrokes('ą');
			expect(keystrokes).toEqual(2);
		});

		it('should return two on uppercaseLetter', function() {
			var line = new Line('a');
			var keystrokes = line.countKeystrokes('X');
			expect(keystrokes).toEqual(2);
		});

		it('should return three on uppercase double keystroke letter', function() {
			var line = new Line('a');
			var keystrokes = line.countKeystrokes('Ż');
			expect(keystrokes).toEqual(3);
		});

		it('should return one for space', function() {
			var line = new Line('a');
			var keystrokes = line.countKeystrokes(' ');
			expect(keystrokes).toEqual(1);
		});

	});

	describe('construction', function() {
		it('should define keystrokesTotal in constructor', function() {
			var line = new Line('Abc');
			expect(line.keystrokesTotal).toEqual(4);
		});
	});

	describe('next expected', function() {

		it('should be first line of text initially', function() {
			var line = new Line('Abc ');
			expect(line.nextExpected).toEqual('A');
		});

		it('should be second without shifts', function() {
			var line = new Line('Abc ');
			line.writtenText = 'A';
			line.updateStatsOnChange();
			expect(line.nextExpected).toEqual('b');
		});

		it('should be third with shift at the beginning', function() {
			var line = new Line('Abc ');
			line.writtenText = 'b';
			line.updateStatsOnChange();
			expect(line.nextExpected).toEqual('c');
		});

		it('should be second when first written wrong', function() {
			var line = new Line('Abc ');
			line.writtenText = 'x';
			line.updateStatsOnChange();
			expect(line.nextExpected).toEqual('b');
		});

		it('should be last when letter missed', function() {
			var line = new Line('Abcd ');
			line.writtenText = 'Ac';
			line.updateStatsOnChange();
			expect(line.nextExpected).toEqual('d');
		});

		it('should work when too many written', function() {
			var line = new Line('Abcdef ');
			line.writtenText = 'Axbc';
			line.updateStatsOnChange();
			expect(line.nextExpected).toEqual('d');
		});

		it('should be space when line finished', function() {
			var line = new Line('Abc ');
			line.writtenText = 'Abc';
			line.updateStatsOnChange();
			expect(line.nextExpected).toEqual(' ');
		});
	});

	describe('keystrokes expected', function() {

		it('should be three without errors', function() {
			var line = new Line('abcde ');
			line.writtenText = 'abc';
			line.updateStatsOnChange();
			expect(line.stats.keystrokesExpected).toEqual(3);
		});

		it('should be three with missed letter', function() {
			var line = new Line('abcde ');
			line.writtenText = 'ac';
			line.updateStatsOnChange();
			expect(line.stats.keystrokesExpected).toEqual(3);
		});

		it('should be three redundant letter', function() {
			var line = new Line('abcde ');
			line.writtenText = 'axbc';
			line.updateStatsOnChange();
			expect(line.stats.keystrokesExpected).toEqual(3);
		});

		it('should equal expected text at the end of line', function() {
			var line = new Line('abc ');
			line.writtenText = 'abc ';
			line.updateStatsOnChange();
			expect(line.stats.keystrokesExpected).toEqual(4);
		});

	});

	describe('keys stats', function() {

		it('should initialize as empty literal', function() {
			var line = new Line('abc ');
			expect(line.keystats).not.toBeNull();
		});

		it('should update wrong letter as first', function() {
			var line = new Line('abc ');
			line.writtenText = 'xbc';
			line.updateStatsOnChange();
			expect(line.keysStats["a"]).toEqual(ERROR_WRONG);
			expect(line.keysStats["x"]).toEqual(ERROR_EXCESSIVE);
		});

		it('should update excessive letter as first', function() {
			var line = new Line('abc ');
			line.writtenText = 'bc';
			line.updateStatsOnChange();
			expect(line.keysStats["a"]).toEqual(ERROR_MISSED);
		});

		it('should update excessive letter as first', function() {
			var line = new Line('abc ');
			line.writtenText = 'xabc';
			line.updateStatsOnChange();
			expect(line.keysStats["x"]).toEqual(ERROR_EXCESSIVE);
			expect(line.keysStats["a"]).toEqual(ERROR_EXCESSIVE);
		});

		it('should update wrong letter', function() {
			var line = new Line('abc ');
			line.writtenText = 'axc';
			line.updateStatsOnChange();
			expect(line.keysStats["b"]).toEqual(ERROR_WRONG);
			expect(line.keysStats["x"]).toEqual(ERROR_EXCESSIVE);
		});

		it('should update missed letter', function() {
			var line = new Line('abc ');
			line.writtenText = 'ac';
			line.updateStatsOnChange();
			expect(line.keysStats["b"]).toEqual(ERROR_MISSED);
		});

		it('should update excessive letter on both it and expected', function() {
			var line = new Line('abc ');
			line.writtenText = 'axbc';
			line.updateStatsOnChange();
			expect(line.keysStats["x"]).toEqual(ERROR_EXCESSIVE);
			expect(line.keysStats["b"]).toEqual(ERROR_EXCESSIVE);
		});

	});

	describe('text appending', function() {

		it('should say that text must be appended', function() {
			// given
			var line = new Line('abc abc abc ');
			line.writtenText = 'abc ';
			// when
			var stats = line.updateStatsOnChange();
			// then
			expect(line.shouldAppendText()).toBe(true);
		});

		it('should say that text must not be appended', function() {
			// given
			var line = new Line('abc abc abc abc abc abc abc abc abc abc abc abc abc abc abc abc abc abc abc abc');
			line.writtenText = 'abc ';
			// when
			var stats = line.updateStatsOnChange();
			// then
			expect(line.shouldAppendText()).toBe(false);
		});

		it('should append text', function() {
			var line = new Line('abc abc');
			var stats = line.appendText(' xyz xyz')
			expect(line.text).toBe('abc abc xyz xyz');
		});

		it('should calculate accuracy corectly', function() {
			var line = new Line('abc abc');
			line.writtenText = 'abc ';
			line.updateStatsOnChange();
			var stats = line.appendText(' xyz xyz')
			line.writtenText = 'abc abc xyz xyz';
			line.updateStatsOnChange();
			expect(line.stats.accuracy).toEqual((100).toFixed(2));
		});

	});

	describe('performance measurment', function() {

		it('should propagate stats response good to true', function() {
			var line = new Line('abc abc');
			spyOn(line.stats, 'isGoodPerformance').andReturn(true);
			expect(line.isGoodPerformance()).toBe(true);
		});

		it('should propagate stats response good to false', function() {
			var line = new Line('abc abc');
			spyOn(line.stats, 'isGoodPerformance').andReturn(false);
			expect(line.isGoodPerformance()).toBe(false);
		});

		it('should propagate stats response bad to true', function() {
			var line = new Line('abc abc');
			spyOn(line.stats, 'isBadPerformance').andReturn(true);
			expect(line.isBadPerformance()).toBe(true);
		});

		it('should propagate stats response bad to false', function() {
			var line = new Line('abc abc');
			spyOn(line.stats, 'isBadPerformance').andReturn(false);
			expect(line.isBadPerformance()).toBe(false);
		});

	});
	describe('focused letter', function() {

		it('should focus on previously wrong letter', function() {
			var line = new Line('abc');
			line.addToKeyStats('a', ERROR_WRONG);
			expect(line.getLetterToPutFocusOn()).toEqual('a');
		});

		it('should unset last wrong letter', function() {
			var line = new Line('abc');
			line.addToKeyStats('a', ERROR_WRONG);
			line.getLetterToPutFocusOn();
			expect(line.getLetterToPutFocusOn()).toEqual('');
		});

		it('should return empty if letter error result less than 1000', function() {
			var line = new Line('abc');
			line.addToKeyStats('a', 555);
			expect(line.getLetterToPutFocusOn()).toEqual('');
		});

		it('should return only letter with highest number of points', function() {
			var line = new Line('abc');
			line.addToKeyStats('a', ERROR_WRONG);
			line.addToKeyStats('b', ERROR_MISSED);
			expect(line.getLetterToPutFocusOn()).toEqual('a');
		});

		it('should divide all error points by factor', function() {
			// given
			var line = new Line('abc');
			line.addToKeyStats('a', ERROR_WRONG);
			line.addToKeyStats('b', ERROR_MISSED);
			// when
			line.fuzzyErrors();
			// then
			expect(line.getLetterToPutFocusOn()).toEqual('');
		});

		it('should return empty string if no errors', function() {
			var line = new Line('abc');
			expect(line.getLetterToPutFocusOn()).toEqual("");
		});

	});
	
	
	describe('errors highlight', function() {

		it('should mark wrong letter', function() {
			var line = new Line('abc');
			line.writtenText = 'axc';
			line.updateStatsOnChange();
			expect(line.textToShow).toEqual(' b ');
		});
		
		it('should mark excessive letter', function() {
			var line = new Line('abc');
			line.writtenText = 'axbc';
			line.updateStatsOnChange();
			expect(line.textToShow).toEqual(' b ');
		});
		
		it('should mark missing letter', function() {
			var line = new Line('abc');
			line.writtenText = 'ac';
			line.updateStatsOnChange();
			expect(line.textToShow).toEqual(' b ');
		});
		
		it('should mark missing letter with shift', function() {
			var line = new Line('abcdef');
			line.writtenText = 'acdf';
			line.updateStatsOnChange();
			expect(line.textToShow).toEqual(' b  e ');
		});
		
		it('should mark wrong space with underscore', function() {
			var line = new Line('ab cd');
			line.writtenText = 'abxcd';
			line.updateStatsOnChange();
			expect(line.textToShow).toEqual('  _  ');
		});

	});

});