describe('stats overall', function() {

	describe('accuracy', function() {

		it('should be 100', function() {
			// given
			var s = new Stats();
			s.keystrokesExpected = 10;
			s.errors = 0;
			s.corrections = 0;
			// when
			s.calculateAccuracy();
			// then
			expect(s.accuracy).toEqual((100).toFixed(2));
		});

		it('should be 90 with one error', function() {
			// given
			var s = new Stats();
			s.keystrokesExpected = 10;
			s.errors = 1;
			s.corrections = 0;
			// when
			s.calculateAccuracy();
			// then
			expect(s.accuracy).toEqual((90).toFixed(2));
		});

		it('should be 90 with one correction', function() {
			// given
			var s = new Stats();
			s.keystrokesExpected = 9;
			s.errors = 0;
			s.corrections = 1;
			// when
			s.calculateAccuracy();
			// then
			expect(s.accuracy).toEqual((90).toFixed(2));
		});

		it('should be 80 with one error and one correction', function() {
			// given
			var s = new Stats();
			s.keystrokesExpected = 9;
			s.errors = 1;
			s.corrections = 1;
			// when
			s.calculateAccuracy();
			// then
			expect(s.accuracy).toEqual((80).toFixed(2));
		});

		it('should not be NaN when line deleted back to zero chars', function() {
			// given
			var s = new Stats();
			s.keystrokesExpected = 0;
			s.errors = 0;
			s.corrections = 0;
			// when
			s.calculateAccuracy();
			// then
			expect(s.accuracy).toEqual(0);
		});

	});

	// describe('result', function() {
	//
	// it('should be half', function() {
	// // given
	// var s = new Stats(0.5);
	// s.wpm = 10;
	// s.accuracy= 1;
	// // when
	// s.updateWithTime();
	// // then
	// expect(s.result).toEqual((5).toFixed(2));
	// });
	// });

	describe('performance measurment - good', function() {

		it('should say good if result at least ' + RESULT_MIN_FOR_GOOD, function() {
			// given
			var s = new Stats();
			s.accuracy = 100;
			s.wpm = RESULT_MIN_FOR_GOOD;
			// then
			expect(s.isGoodPerformance()).toBe(true);
		});

		it('should say good if accuracy at least ' + ACCURACY_MIN_FOR_GOOD + ' and result at least '
		        + RESULT_MIN_FOR_GOOD, function() {
			// given
			var s = new Stats();
			s.accuracy = ACCURACY_MIN_FOR_GOOD;
			s.wpm = 100;
			// then
			expect(s.isGoodPerformance()).toBe(true);
		});

		it('should say not good if accuracy below ' + ACCURACY_MIN_FOR_GOOD, function() {
			// given
			var s = new Stats();
			s.accuracy = ACCURACY_MIN_FOR_GOOD - 1;
			s.wpm = 630;
			// then
			expect(s.isGoodPerformance()).toBe(false);
		});

		it('should say not good if result below ' + RESULT_MIN_FOR_GOOD, function() {
			// given
			var s = new Stats();
			s.accuracy = 95;
			s.wpm = RESULT_MIN_FOR_GOOD;
			// then
			expect(s.isGoodPerformance()).toBe(false);
		});

		it('should say not good if result went down with one letter added', function() {
			// given
			var s = new Stats();
			s.accuracy = 95;
			s.wpm = 40;
			var firstAddedLetter = s.isGoodPerformance();// this is true
			s.wpm = 33;
			// then
			expect(firstAddedLetter).toBe(true);
			expect(s.isGoodPerformance()).toBe(false);
		});
		
		it('should not add new letter when it went below min result', function() {
			// given
			var s = new Stats();
			s.accuracy = 95;
			s.wpm = 31;
			var firstAddedLetter = s.isGoodPerformance();// this is true
			s.wpm = 28;
			// then
			expect(firstAddedLetter).toBe(true);
			expect(s.isGoodPerformance()).toBe(false);
		});
		
		it('should add new letter when after adding first the speed was maintained', function() {
			// given
			var s = new Stats();
			s.accuracy = 95;
			s.wpm = 31;
			var firstAddedLetter = s.isGoodPerformance();// this is true
			s.wpm = 31;
			// then
			expect(firstAddedLetter).toBe(true);
			expect(s.isGoodPerformance()).toBe(true);
		});

	});

	describe('performance measurment - bad', function() {

		it('should say bad if result below ' + RESULT_MAX_FOR_BAD, function() {
			// given
			var s = new Stats();
			s.accuracy = 100;
			s.wpm = RESULT_MAX_FOR_BAD - 1;
			// then
			expect(s.isBadPerformance()).toBe(true);
		});

		it('should say bad not regarding wpm if accuracy below ' + ACCURACY_MAX_FOR_BAD, function() {
			// given
			var s = new Stats();
			s.accuracy = ACCURACY_MAX_FOR_BAD - 1;
			s.wpm = 200;
			// then
			expect(s.isBadPerformance()).toBe(true);
		});

		it('should say not bad if accuracy at least ' + ACCURACY_MAX_FOR_BAD + 'and result at least '
		        + RESULT_MAX_FOR_BAD, function() {
			// given
			var s = new Stats();
			s.accuracy = ACCURACY_MIN_FOR_GOOD;
			s.wpm = 100;
			// then
			expect(s.isBadPerformance()).toBe(false);
		});

		it('should say not bad if result at least ' + RESULT_MAX_FOR_BAD, function() {
			// given
			var s = new Stats();
			s.accuracy = ACCURACY_MAX_FOR_BAD;
			s.wpm = RESULT_MAX_FOR_BAD + 20;
			// then
			expect(s.isBadPerformance()).toBe(false);
		});

	});

});