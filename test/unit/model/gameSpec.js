describe('game', function() {

	describe('updates on letter typed', function() {

		it('should be 100 accuracy', function() {
			// given
			var game = new Game();
			// when
			game.letterTyped("a");
			// then
			expect(game.line.stats.accuracy).toEqual((100).toFixed(2));
		});

		it('should be 50 accuracy when one error commited', function() {
			// given
			var game = new Game();
			// when
			game.addExcessiveError("b");
			game.letterTyped("b");// called together with excessive error
			game.letterTyped("a");
			// then
			expect(game.line.stats.accuracy).toEqual((50).toFixed(2));
		});

		it('should be 3 errors on one excessive and one missed (double calculated)', function() {
			// given
			var game = new Game();
			// when
			game.addExcessiveError("b");
			game.addMissedError("c");
			// then
			expect(game.line.stats.errors).toEqual(3);
		});

	});

	describe('updates with time', function() {

		it('should do nothing when first called', function() {
			// given
			var game = new Game();
			// when
			game.letterTyped("a");
			game.updateWithTime();
			// then
			expect(game.line.stats.wpm).toEqual((0).toFixed(2));
		});

		it('should calculate 12 wpm when one letter withing one second typed', function() {
			// given
			var game = new Game();
			// when
			game.letterTyped("a");
			game.updateWithTime();
			game.updateWithTime();
			// then
			expect(game.line.stats.wpm).toEqual((12).toFixed(2));
		});

		it('should not fail when update with time without anything typed', function() {
			// given
			var game = new Game();
			// when
			game.updateWithTime();
			game.updateWithTime();
			// then
			expect(game.line.stats.wpm).toEqual((0).toFixed(2));
		});

		it('should not fail when only errors commited without anything typed', function() {
			// given
			var game = new Game();
			// when
			game.addExcessiveError("b");
			game.addMissedError("c");
			game.updateWithTime();
			game.updateWithTime();
			// then
			expect(game.line.stats.wpm).toEqual((0).toFixed(2));
		});

	});

	describe('level', function() {

		it('should be one when one letter typed', function() {
			// given
			var game = new Game();
			// when
			game.letterTyped("a");
			game.updateLevel();
			// then
			expect(game.level).toEqual(1);
		});

		it('should be one two when 11 keystrokes typed', function() {
			// given
			var game = new Game();
			// when
			game.line.stats.keystrokes = 11;
			game.updateLevel();
			// then
			expect(game.level).toEqual(2);
		});
		it('should be one two when 25 keystrokes typed', function() {
			// given
			var game = new Game();
			// when
			game.line.stats.wpm = 12;
			game.line.stats.accuracy = 100;
			game.line.stats.keystrokes = 11;
			game.line.stats.errors = 2;
			game.updateLevel();
			// then
			expect(game.level).toEqual(2);
		});

	});

});