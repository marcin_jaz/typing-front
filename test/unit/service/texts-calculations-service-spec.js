'use strict';

describe('TextsCalculationsService', function () {
    beforeEach(module('myApp.services'));

    var $service;
    beforeEach(inject(function (textsCalculationsService) {
        $service = textsCalculationsService;
    }));

    describe('shouldAddFocus learn', function () {

        it('should return true', function () {
            //given
            var letters = ["a", "i", "e", "z", "o", "n", "r", "y", "s"];
            var config = {
                focus_letter: "z",
                use_focus: 1,
                learn_config: 7
            }
            //when
            var r = $service.shouldAddFocusForLearn(letters, config);
            //then
            expect(r).toBe(true);
        });

        it('should return false when empty focus letter', function () {
            //given
            var letters = ["a", "i", "e", "z", "o", "n", "r", "y", "s"];
            var config = {
                focus_letter: " ",
                use_focus: 1,
                learn_config: 7
            }
            //when
            var r = $service.shouldAddFocusForLearn(letters, config);
            //then
            expect(r).toBe(false);
        });

        it('should return false when use focus configured to false', function () {
            //given
            var letters = ["a", "i", "e", "z", "o", "n", "r", "y", "s"];
            var config = {
                focus_letter: "i",
                use_focus: 0,
                learn_config: 7
            }
            //when
            var r = $service.shouldAddFocusForLearn(letters, config);
            //then
            expect(r).toBe(false);
        });

        it('should return false when focus letter not in config yet', function () {
            //given
            var letters = ["a", "i", "e", "z", "o", "n", "r", "y", "s"];
            var config = {
                focus_letter: "s",
                use_focus: 1,
                learn_config: 7
            }
            //when
            var r = $service.shouldAddFocusForLearn(letters, config);
            //then
            expect(r).toBe(false);
        });

    });

    describe('insert specials', function () {

        it('should only add dot as it is last', function () {
            //given
            var words = [{"v": "a"}, {"v": "b"}, {"v": "c"}, {"v": "d"}, {"v": "e"}, {"v": "f"}, {"v": "g"}, {"v": "h"},
                {"v": "i"}, {"v": "j"}, {"v": "k"}, {"v": "l"}, {"v": "m"}, {"v": "n"}, {"v": "o"}];//length=15
            var specials = [new SpecialChar({"v": ",", "r": "0001"})];
            //when
            var r = $service.insertSpecialCharsBuildingString(words, specials, '');
            //then
            expect(r.split(",").length - 1).toEqual(6);
        });

        it('should also add not the last one', function () {
            //given
            var words = [{"v": "a"}, {"v": "b"}, {"v": "c"}, {"v": "d"}, {"v": "e"}, {"v": "f"}, {"v": "g"}, {"v": "h"},
                {"v": "i"}, {"v": "j"}, {"v": "k"}, {"v": "l"}, {"v": "m"}, {"v": "n"}, {"v": "o"}];//length=15
            var specials = [new SpecialChar({"v": ",", "r": "0001"}), new SpecialChar({"v": ".", "r": "1001"})];
            //when
            var r = $service.insertSpecialCharsBuildingString(words, specials, '');
            //then
            expect(r.split(",").length - 1).toEqual(3);
        });

        it('should also add two previous ones once', function () {
            //given
            var words = [{"v": "a"}, {"v": "b"}, {"v": "c"}, {"v": "d"}, {"v": "e"}, {"v": "f"}, {"v": "g"}, {"v": "h"},
                {"v": "i"}, {"v": "j"}, {"v": "k"}, {"v": "l"}, {"v": "m"}, {"v": "n"}, {"v": "o"}];//length=15
            var specials = [new SpecialChar({"v": ";", "r": "0001"}), new SpecialChar({
                "v": ",",
                "r": "0001"
            }), new SpecialChar({"v": ".", "r": "1001"})];
            //when
            var r = $service.insertSpecialCharsBuildingString(words, specials, '');
            //then
            expect(r.split(";").length - 1).toEqual(1);
            expect(r.split(",").length - 1).toEqual(2);
        });

        it('should include focus and make it twice', function () {
            //given
            var words = [{"v": "a"}, {"v": "b"}, {"v": "c"}, {"v": "d"}, {"v": "e"}, {"v": "f"}, {"v": "g"}, {"v": "h"},
                {"v": "i"}, {"v": "j"}, {"v": "k"}, {"v": "l"}, {"v": "m"}, {"v": "n"}, {"v": "o"}];//length=15
            var specials = [new SpecialChar({"v": ";", "r": "0001"}), new SpecialChar({
                "v": ",",
                "r": "0001"
            }), new SpecialChar({"v": ".", "r": "1001"})];
            //when
            var r = $service.insertSpecialCharsBuildingString(words, specials, ',');
            //then
            expect(r.split(",").length - 1).toEqual(2);
        });

        it('should include all others once', function () {
            //given
            var words = [{"v": "a"}, {"v": "b"}, {"v": "c"}, {"v": "d"}, {"v": "e"}, {"v": "f"}, {"v": "g"}, {"v": "h"},
                {"v": "i"}, {"v": "j"}, {"v": "k"}, {"v": "l"}, {"v": "m"}, {"v": "n"}, {"v": "o"}];//length=15
            var specials = [
                new SpecialChar({"v": ":", "r": "0001"}),
                new SpecialChar({"v": ";", "r": "0001"}),
                new SpecialChar({"v": ",", "r": "0001"}),
                new SpecialChar({"v": ".", "r": "1001"})];
            //when
            var r = $service.insertSpecialCharsBuildingString(words, specials, '');
            //then
            expect(r.split(",").length - 1).toEqual(1);
            expect(r.split(";").length - 1).toEqual(1);
            expect(r.split(":").length - 1).toEqual(1);
        });

        it('should include one random from before 4 last added', function () {
            //given
            var words = [{"v": "a"}, {"v": "b"}, {"v": "c"}, {"v": "d"}, {"v": "e"}, {"v": "f"}, {"v": "g"}, {"v": "h"},
                {"v": "i"}, {"v": "j"}, {"v": "k"}, {"v": "l"}, {"v": "m"}, {"v": "n"}, {"v": "o"}];//length=15
            var specials = [
                new SpecialChar({"v": "-", "r": "0011"}),
                new SpecialChar({"v": "()", "r": "0)10"}),
                new SpecialChar({"v": ":", "r": "0001"}),
                new SpecialChar({"v": ";", "r": "0001"}),
                new SpecialChar({"v": ",", "r": "0001"}),
                new SpecialChar({"v": ".", "r": "1001"})];
            //when
            var r = $service.insertSpecialCharsBuildingString(words, specials, '');
            //then
            expect(r.split("-").length - 1 + r.split("(").length - 1).toEqual(1);
        });

        it('should close the parenthesis', function () {
            //given
            var words = [{"v": "a"}, {"v": "b"}, {"v": "c"}, {"v": "d"}, {"v": "e"}, {"v": "f"}, {"v": "g"}, {"v": "h"},
                {"v": "i"}, {"v": "j"}, {"v": "k"}, {"v": "l"}, {"v": "m"}, {"v": "n"}, {"v": "o"}];//length=15
            var specials = [new SpecialChar({"v": "()", "r": "0)10"})];
            //when
            var r = $service.insertSpecialCharsBuildingString(words, specials, '');
            //then
            expect(r.split("(").length - 1).toEqual(6);
            expect(r.split(")").length - 1).toEqual(6);
        });

    });

});
