'use strict';

describe('ApproachService', function () {
    beforeEach(module('myApp.services'));
    beforeEach(function () {
        //mock unused xdLocalStorage because here it won't be visible
        angular.module('myApp.services').service('xdLocalStorage', function () {
        });
    });

    var $service;
    beforeEach(inject(function (approachService) {
        $service = approachService;
    }));

    describe('getResultFactorForWrite', function () {

        it('should return 1 when more than 5000 words', function () {
            var factor = $service.getResultFactorForWrite(6000);
            expect(factor).toEqual(1);
        });

        it('should return 0.95 when 5000 words', function () {
            var factor = $service.getResultFactorForWrite(5000);
            expect(factor).toEqual(0.95);
        });

        it('should return 0.85 when 3000 words', function () {
            var factor = $service.getResultFactorForWrite(3000);
            expect(factor).toEqual(0.85);
        });

        it('should return 0.75 when 1000 words', function () {
            var factor = $service.getResultFactorForWrite(1000);
            expect(factor).toEqual(0.75);
        });

    });
});
