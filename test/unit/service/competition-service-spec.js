'use strict';

/* jasmine specs for services go here */

describe('CompetitionService', function() {
	beforeEach(module('myApp.services'));

	var $service;
	beforeEach(inject(function(competitionService) {
		$service = competitionService;
	}));

	var CURRENT_USER = {
		id : 999 };
	var HOW_MANY_TO_SHOW = 3;

	describe('calculatePositions', function() {

		it('should show only last 3 users', function() {
			// given
			var initialPositions = [ {
			id : 1,
			best_result : 12 }, {
			id : 2,
			best_result : 10 }, {
			id : 3,
			best_result : 8 }, {
			id : 4,
			best_result : 6 } ];
			var result = 2;
			// when
			var positions = $service.calculatePositions(initialPositions, CURRENT_USER, result, HOW_MANY_TO_SHOW);
			// then
			expect(positions.length).toEqual(3);
			expect(positions[0].id).toEqual(3);
			expect(positions[1].id).toEqual(4);
			expect(positions[2].id).toEqual(CURRENT_USER.id);
		});

		it('should show only first 3 users', function() {
			// given
			var initialPositions = [ {
			id : 1,
			best_result : 12 }, {
			id : 2,
			best_result : 10 }, {
			id : 3,
			best_result : 8 }, {
			id : 4,
			best_result : 6 } ];
			var result = 22;
			// when
			var positions = $service.calculatePositions(initialPositions, CURRENT_USER, result, HOW_MANY_TO_SHOW);
			// then
			expect(positions.length).toEqual(3);
			expect(positions[0].id).toEqual(CURRENT_USER.id);
			expect(positions[1].id).toEqual(1);
			expect(positions[2].id).toEqual(2);
		});

		it('should show only 3 users in the middle', function() {
			// given
			var initialPositions = [ {
			id : 1,
			best_result : 12 }, {
			id : 2,
			best_result : 10 }, {
			id : 3,
			best_result : 8 }, {
			id : 4,
			best_result : 6 } ];
			var result = 9;
			// when
			var positions = $service.calculatePositions(initialPositions, CURRENT_USER, result, HOW_MANY_TO_SHOW);
			// then
			expect(positions.length).toEqual(3);
			expect(positions[0].id).toEqual(2);
			expect(positions[1].id).toEqual(CURRENT_USER.id);
			expect(positions[2].id).toEqual(3);
		});

		it('should show only 3 users in the middle when having current user twice', function() {
			// given
			var initialPositions = [ {
			id : 1,
			best_result : 12 }, {
			id : CURRENT_USER.id,
			best_result : 10 }, {
			id : 3,
			best_result : 8 }, {
			id : 4,
			best_result : 6 } ];
			var result = 9;
			// when
			var positions = $service.calculatePositions(initialPositions, CURRENT_USER, result, HOW_MANY_TO_SHOW);
			// then
			expect(positions.length).toEqual(3);
			expect(positions[0].id).toEqual(CURRENT_USER.id);
			expect(positions[1].id).toEqual(CURRENT_USER.id);
			expect(positions[2].id).toEqual(3);
		});

		it('should show only last 3 users when having current user twice and last', function() {
			// given
			var initialPositions = [ {
			id : 1,
			best_result : 12 }, {
			id : 2,
			best_result : 10 }, {
			id : 4,
			best_result : 8 }, {
			id : CURRENT_USER.id,
			best_result : 6 } ];
			var result = 2;
			// when
			var positions = $service.calculatePositions(initialPositions, CURRENT_USER, result, HOW_MANY_TO_SHOW);
			// then
			expect(positions.length).toEqual(3);
			expect(positions[0].id).toEqual(4);
			expect(positions[1].id).toEqual(CURRENT_USER.id);
			expect(positions[2].id).toEqual(CURRENT_USER.id);
		});

	});

});
