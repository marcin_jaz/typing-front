'use strict';

/* jasmine specs for filters go here */

describe('filter', function() {
	beforeEach(module('myApp.filters'));

	beforeEach(function() {
		var mockTranslateFilter = function(key) {
			if (key === 'MINUTE_ONE_LETTER') {
				return 'm';
			} else if (key === 'NO_LIMIT') {
				return 'no limit';
			}
		};
		module(function($provide) {
			$provide.value('translateFilter', mockTranslateFilter);
		});

	});

	describe('write words or no limit', function() {

		it('should return number if less than 5000', inject(function(writeConfigFilter) {
			var filtered = writeConfigFilter(3000);
			expect(filtered).toBe(3000);
		}));

		it('should return no limit', inject(function(writeConfigFilter) {
			var filtered = writeConfigFilter(6000);
			expect(filtered).toBe('no limit');
		}));
	});

	describe('learn config display', function() {

		it('should return for use focus equal to true', inject(function(learnConfigDisplayFilter) {
			// given
			var config = {
			learn_minutes : 2,
			learn_config : 5,
			letters_number : 26,
			use_focus : "1" };
			// when
			var filtered = learnConfigDisplayFilter(config);
			// then
			expect(filtered).toBe("2m, 6/26, " + SIGN_TRUE);
		}));

		it('should return for use focus equal to false', inject(function(learnConfigDisplayFilter) {
			// given
			var config = {
			learn_minutes : 2,
			learn_config : 5,
			letters_number : 26,
			use_focus : "0" };
			// when
			var filtered = learnConfigDisplayFilter(config);
			// then
			expect(filtered).toBe("2m, 6/26, " + SIGN_FALSE);
		}));
	});

	describe('learn config display', function() {

		it('should return for use focus equal to true', inject(function(writeConfigDisplayFilter) {
			// given
			var config = {
			write_minutes : 3,
			write_config : 1000,
			use_focus : "1" };
			// when
			var filtered = writeConfigDisplayFilter(config);
			// then
			expect(filtered).toBe("3m, 1000, " + SIGN_TRUE);
		}));

		it('should return for use focus equal to false', inject(function(writeConfigDisplayFilter) {
			// given
			var config = {
			write_minutes : 3,
			write_config : 1000,
			use_focus : "0" };
			// when
			var filtered = writeConfigDisplayFilter(config);
			// then
			expect(filtered).toBe("3m, 1000, " + SIGN_FALSE);
		}));
	});

});
