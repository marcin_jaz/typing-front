var initializeUser = function () {
};
var ROUTES = {

    index: {
        val: {
            templateUrl: 'partials/main.html',
            controller: 'MainController',
            resolve: initializeUser
        },
        params: '',
        langs: {
            en: '',
            es: '',
            pl: ''
        }
    },

    learn: {
        val: {
            templateUrl: 'partials/learn.html',
            controller: 'LearnController',
            resolve: initializeUser
        },
        params: '',
        langs: {
            en: 'learn-touch-typing',
            es: 'aprender-mecanografia',
            pl: 'nauka-szybkiego-pisania'
        }
    },

    write: {
        val: {
            templateUrl: 'partials/write.html',
            controller: 'WriteController',
            resolve: initializeUser
        },
        params: '',
        langs: {
            en: 'test-typing-speed',
            es: 'test-velocidad-al-teclado',
            pl: 'test-szybkosci-pisania'
        }
    },

    competitions: {
        val: {
            templateUrl: 'partials/competitions.html',
            controller: 'CompetitionsController',
            resolve: initializeUser
        },
        params: '',
        langs: {
            en: 'touch-typing-competitions',
            es: 'competitiones-en-mecanografia',
            pl: 'zawody-w-pisaniu-na-klawiaturze'
        }
    },

    competition: {
        val: {
            templateUrl: 'partials/competition-write.html',
            controller: 'CompetitionController',
            resolve: initializeUser
        },
        params: '/:competitionId/:key?',
        langs: {
            en: 'touch-typing-competition',
            es: 'competition-en-mecanografia',
            pl: 'szybkie-pisanie-zawody'
        }
    },

    login: {
        val: {
            templateUrl: 'partials/login.html',
            controller: 'LoginController',
            resolve: initializeUser
        },
        params: '',
        langs: {
            en: 'sign-in-for-touch-typing',
            es: 'inicia-session-para-mecanografia',
            pl: 'zaloguj-sie-do-pisania-bezwzrokowego'
        }
    },

    dashboard: {
        val: {
            templateUrl: 'partials/dashboard.html',
            controller: 'DashboardController',
            resolve: initializeUser
        },
        params: '',
        langs: {
            en: 'your-dashboard',
            es: 'tu-perfil',
            pl: 'twoj-profil'
        }
    },

    profile: {
        val: {
            templateUrl: 'partials/dashboard-other.html',
            controller: 'DashboardOtherController',
            resolve: initializeUser
        },
        params: '/:id',
        langs: {
            en: 'user-touch-typing-statistics',
            es: 'estadisticas-de-mecanografia-del-usuario',
            pl: 'statystyki-pisania-uzytkownika'
        }
    },

    falling_letters: {
        val: {
            templateUrl: 'partials/games/falling-letters.html',
            controller: 'FallingLettersController',
            resolve: initializeUser
        },
        params: '',
        langs: {
            en: 'games-to-learn-touch-typing-falling-letters',
            es: 'juegos-para-aprender-mecanografia-letras-que-caen',
            pl: 'gry-do-nauki-pisania-bezwzorkowego-spadajace-litery'
        }
    },

    benefits: {
        val: {
            templateUrl: 'partials/articles/article.html',
            controller: 'ArticleController',
            resolve: initializeUser
        },
        params: '',
        langs: {
            en: 'touch-typing-benefits',
            es: 'mecanografia-beneficios',
            pl: 'pisanie-bezwzrokowe-korzysci'
        }
    },

    howtolearn: {
        val: {
            templateUrl: 'partials/articles/article.html',
            controller: 'ArticleController',
            resolve: initializeUser
        },
        params: '',
        langs: {
            en: 'how-to-learn-touch-typing',
            es: 'como-aprender-mecanografia',
            pl: 'jak-uczyc-sie-szybkiego-pisania-bezwzrokowego'
        },
        title: {
            pl: 'Jak uczyć się pisania bezwzrokowego',
            es: 'Como aprender mecanografía',
            en: 'How to learn touch typing'
        }
    },


    school: {
        val: {
            templateUrl: 'partials/school/school.html',
            controller: 'SchoolController',
            resolve: initializeUser
        },
        params: '',
        langs: {
            en: 'touch-typing-school',
            es: 'escuela-de-mecanografia',
            pl: 'szkolenie-z-pisania-bezwzrokowego'
        },
        title: {
            pl: 'Szkolenie z pisania bezwzrokowego',
            es: 'Curso de mecanografia en grupo',
            en: 'Keyboard typing school'
        }
    }

};

var removeLang = function (requestUri) {
    var lang = requestUri.slice(requestUri.indexOf('=') + 1, requestUri.length);
    var index = requestUri.indexOf('?lang');
    return requestUri.slice(0, index) + '-' + lang;
};

var getUrlsForLang = function (lang) {
    return ['?lang=' + lang,
        ROUTES.learn.langs[lang] + '?lang=' + lang,
        ROUTES.write.langs[lang] + '?lang=' + lang,
        ROUTES.competitions.langs[lang] + '?lang=' + lang,
        ROUTES.login.langs[lang] + '?lang=' + lang,
        ROUTES.benefits.langs[lang] + '?lang=' + lang,
        ROUTES.howtolearn.langs[lang] + '?lang=' + lang,
        ROUTES.school.langs[lang] + '?lang=' + lang]
};

// https://www.npmjs.com/package/grunt-html-snapshot
var getSnapshotConfigForLang = function (lang) {
    return {
        options: {
            snapshotPath: 'dist/snapshots/',
            sitePath: 'http://' + lang + '.localhost:90/typing1/dist/',
            fileNamePrefix: '',
            msWaitForPages: 1000,
            sanitize: removeLang,
            urls: getUrlsForLang(lang),
            removeScripts: true,
            replaceStrings: [{
                '/typing1/dist': ''
            }]

        }

    };
}

module.exports =
    function (grunt) {

        grunt.initConfig({
            pkg: grunt.file.readJSON('package.json'),

            clean: ["dist", ".tmp"],

            copy: {
                main: {
                    expand: true,
                    cwd: 'app/',
                    src: ['**', '!js/**', '!lib/**', '!**/*.css', '!**/*.html', 'index.html', 'local-storage.html', '.htaccess', 'js/xd-local-storage-post-message-api.min.js', 'js/ads.js'],
                    dest: 'dist/'
                }

            },

            htmlmin: {
                partials: {
                    options: {
                        removeComments: true,
                        collapseWhitespace: true
                    },
                    files: [{
                        expand: true,
                        cwd: 'app/partials',
                        src: '**/*.html',
                        dest: 'dist/partials'
                    }]
                },

                index: {
                    options: {
                        removeComments: true,
                        collapseWhitespace: true
                    },
                    files: [{
                        src: 'dist/index.html',
                        dest: 'dist/index.html'
                    }]
                }

            },

            rev: {
                files: {
                    src: ['dist/**/*.{js,css}', '!dist/js/shims/**']

                }

            },

            useminPrepare: {
                html: 'app/index.html'
            },

            usemin: {
                html: ['dist/index.html']
            },

            uglify: {
                options: {
                    report: 'min',
                    mangle: true,
                    preserveComments: false
                }
            },

            htmlSnapshot: {
                en: getSnapshotConfigForLang('en'),
                pl: getSnapshotConfigForLang('pl'),
                es: getSnapshotConfigForLang('es')

            },

//https://github.com/gruntjs/grunt-contrib-compress
            compress: {
                js: {
                    options: {
                        mode: 'deflate'
                    },
                    files: [{
                        expand: true,
                        src: ['dist/js/*.js']
                    }]
                },
                css: {
                    options: {
                        mode: 'deflate'
                    },
                    files: [{
                        expand: true,
                        src: ['dist/css/*.css']
                    }]
                },
                html: {
                    options: {
                        mode: 'deflate'
                    },
                    files: [{
                        expand: true,
                        src: ['dist/partials/*.html','dist/partials/**/*.html', 'dist/index.html']
                    }]
                },
                json: {
                    options: {
                        mode: 'deflate'
                    },
                    files: [{
                        expand: true,
                        src: ['dist/data/**/*.json','dist/data/*.json']
                    }]
                }
            },

            'string-replace': {
                dist: {
                    files: {
                        'dist/index.html': 'dist/index.html'
                    },
                    options: {
                        replacements: [{
                            pattern: '\/typing1\/app\/',
                            replacement: '\/typing1\/dist\/'
                        }]
                    }
                },

                production: {
                    files: {
                        'dist/index.html': 'dist/index.html'
                    },
                    options: {
                        replacements: [
                            // place files inline example
                            {
                                pattern: '\/typing1\/dist\/',
                                replacement: '/'
                            }]
                    }
                }

            }

        });

        grunt.loadNpmTasks('grunt-contrib-clean');
        grunt.loadNpmTasks('grunt-contrib-copy');
        grunt.loadNpmTasks('grunt-contrib-concat');
        grunt.loadNpmTasks('grunt-contrib-cssmin');
        grunt.loadNpmTasks('grunt-contrib-uglify');
        grunt.loadNpmTasks('grunt-rev');
        grunt.loadNpmTasks('grunt-usemin');
        grunt.loadNpmTasks('grunt-contrib-htmlmin');
        grunt.loadNpmTasks('grunt-string-replace');
        grunt.loadNpmTasks('grunt-html-snapshot');
        grunt.loadNpmTasks('grunt-contrib-compress');

        grunt.registerTask(
            'default',
            ['clean', 'copy', 'htmlmin:partials', 'useminPrepare', 'concat', 'uglify', 'cssmin',
                'rev', 'usemin', 'clean:1', 'string-replace:dist', 'htmlmin:index',
                'compress:js','compress:css','compress:html','compress:json',
                'htmlSnapshot:en', 'htmlSnapshot:pl', 'htmlSnapshot:es',
                'string-replace:production'
            ]);

        grunt
            .registerTask(
                'no-snapshots',
                ['clean', 'copy', 'htmlmin:partials', 'useminPrepare', 'concat', 'uglify', 'cssmin',
                    'rev', 'usemin', 'clean:1', 'string-replace:dist', 'htmlmin:index',
                    'string-replace:production'
                ]);


        grunt
            .registerTask(
                'local',
                ['clean', 'copy', 'htmlmin:partials', 'useminPrepare', 'concat', 'uglify', 'cssmin',
                    'rev', 'usemin', 'clean:1', 'string-replace:dist','htmlmin:index'
                   // , 'compress:json'

                ]);

    };